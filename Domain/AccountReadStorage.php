<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

interface AccountReadStorage
{
    public function getAccount(int $accountId): Account;
}