<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

interface TopCityReadStorage
{
    public function isBigCity(int $id): bool;
}