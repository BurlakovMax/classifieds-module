<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;
use LazyLemurs\Structures\PhoneNumber;

/**
 * @ORM\Entity(repositoryClass="App\Escorts\Infrastructure\Persistence\PhoneBlacklistDoctrineRepository")
 * @ORM\Table(name="phone_blacklist")
 */
class PhoneBlacklist
{
    /**
     * @ORM\Column(name="id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="phone", type="phone_number")
     */
    private PhoneNumber $phone;

    /**
     * @ORM\Column(name="reason", type="string", length=255)
     */
    private string $reason;

    /**
     * @ORM\Column(name="created_stamp", type="timestamp", length=11)
     */
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $createdBy;

    public function __construct(PhoneNumber $phone, string $reason, int $createdBy)
    {
        $this->id = 0;
        $this->phone = $phone;
        $this->reason = $reason;
        $this->createdAt = new \DateTimeImmutable();
        $this->createdBy = $createdBy;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPhone(): PhoneNumber
    {
        return $this->phone;
    }

    public function getReason(): string
    {
        return $this->reason;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedBy(): int
    {
        return $this->createdBy;
    }
}
