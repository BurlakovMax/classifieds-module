<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

interface EscortSponsorWriteStorage
{
    public function add(EscortSponsor $escortSponsor): void;

    public function getAndLockByEscortIdAndLocationId(int $escortId, int $locationId): ?EscortSponsor;
}