<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

final class City
{
    private int $id;

    private string $name;

    private string $url;

    private ?string $description;

    /**
     * @var array|string[]
     */
    private array $images;

    private ?string $video;

    private bool $active;

    private ?bool $isSignificant;

    private ?int $countryId;

    private ?int $stateId;

    private ?string $stateName;

    private ?string $countryName;

    /**
     * @param string[] $images
     */
    public function __construct(
        int $id,
        string $name,
        string $url,
        ?string $description,
        array $images,
        ?string $video,
        bool $active,
        ?bool $isSignificant,
        ?int $countryId,
        ?int $stateId,
        ?string $stateName,
        ?string $countryName
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->url = $url;
        $this->description = $description;
        $this->images = $images;
        $this->video = $video;
        $this->active = $active;
        $this->isSignificant = $isSignificant;
        $this->countryId = $countryId;
        $this->stateId = $stateId;
        $this->stateName = $stateName;
        $this->countryName = $countryName;
    }


    public function getStateName(): ?string
    {
        return $this->stateName;
    }

    public function getCountryName(): ?string
    {
        return $this->countryName;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string[]
     */
    public function getImages(): array
    {
        return $this->images;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function getIsSignificant(): ?bool
    {
        return $this->isSignificant;
    }

    public function getCountryId(): ?int
    {
        return $this->countryId;
    }

    public function getStateId(): ?int
    {
        return $this->stateId;
    }
}