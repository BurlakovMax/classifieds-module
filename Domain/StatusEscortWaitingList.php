<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\DomainSupport\Enumerable;

final class StatusEscortWaitingList extends Enumerable
{
    public static function pending(): self
    {
        return self::createEnum(1);
    }

    public static function reserved(): self
    {
        return self::createEnum(2);
    }

    public static function notified(): self
    {
        return self::createEnum(3);
    }

    public static function canceled(): self
    {
        return self::createEnum(4);
    }

    public static function error(): self
    {
        return self::createEnum(5);
    }
}