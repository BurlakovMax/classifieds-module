<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

interface CityReadStorage
{
    public function getById(int $id): City;

    /**
     * @param int ...$ids
     * @return City[]
     */
    public function getByIds(int ...$ids): array;
}