<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

interface EscortClickStorage
{
    /**
     * @return EscortClickStatistic[]
     */
    public function getByEscortId(int $escortId, \DateTimeImmutable $dateFrom, \DateTimeImmutable $dateTo): array;

    public function getAndLockById(int $escortId, int $locationId, string $date): ?EscortClickStatistic;

    public function add(EscortClickStatistic $statistic): void;
}
