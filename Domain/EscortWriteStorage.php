<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

interface EscortWriteStorage
{
    public function getAndLock(int $id): ?Escort;

    public function add(Escort $escort): void;
}
