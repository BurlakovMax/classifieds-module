<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use LazyLemurs\DomainEvents\DomainEvent;

final class FileUploaded extends DomainEvent
{
    private string $name;

    private string $path;

    private string $saveToPath;

    public function __construct(string $name, string $path, string $saveToPath)
    {
        parent::__construct();
        $this->name = $name;
        $this->path = $path;
        $this->saveToPath = $saveToPath;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getSaveToPath(): string
    {
        return $this->saveToPath;
    }
}