<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use LazyLemurs\DomainEvents\DomainEvent;
use LazyLemurs\Structures\Email;

final class EmailToEscortSent extends DomainEvent
{
    private Email $email;

    private Email $replyTo;

    private string $message;

    public function __construct(Email $email, Email $replyTo, string $message)
    {
        parent::__construct();

        $this->email = $email;
        $this->replyTo = $replyTo;
        $this->message = $message;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getReplyTo(): Email
    {
        return $this->replyTo;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
