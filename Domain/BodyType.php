<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use LitGroup\Enumerable\Enumerable;

final class BodyType extends Enumerable
{
    public static function tiny(): self
    {
        return self::createEnum(1);
    }

    public static function slim(): self
    {
        return self::createEnum(2);
    }

    public static function athletic(): self
    {
        return self::createEnum(3);
    }

    public static function average(): self
    {
        return self::createEnum(4);
    }

    public static function curvy(): self
    {
        return self::createEnum(5);
    }

    public static function bbw(): self
    {
        return self::createEnum(6);
    }

    /**
     * @deprecated For legacy support only
     */
    public static function none(): self
    {
        return self::createEnum(0);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getRawValue();
    }
}
