<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\ApplicationSupport\AccessControl\AccessForbidden;
use App\Escorts\Application\CreateEscortCommand;
use App\Escorts\Application\EscortNotFound;
use App\Escorts\Application\UpdateEscortAdminCommand;
use App\Escorts\Application\UpdateEscortCommand;
use LazyLemurs\DomainEvents\PublisherContainer;
use LazyLemurs\Structures\Email;
use Ramsey\Uuid\Uuid;

final class EscortService
{
    private EscortReadStorage $escortReadStorage;

    private EscortWriteStorage $escortWriteStorage;

    private EscortLinkTokenStorage $escortLinkTokenStorage;

    private TimezoneReadStorage $timezoneReadStorage;

    private AccountServiceProviderInterface $accountServiceProvider;

    private CityReadStorage $cityReadStorage;

    private EscortSponsorWriteStorage $escortSponsorWriteStorage;

    private EscortSideWriteStorage $escortSideWriteStorage;

    private EscortStickyWriteStorage $escortStickyWriteStorage;

    private int $expireInDays;

    public function __construct(
        EscortReadStorage $escortReadStorage,
        EscortWriteStorage $escortWriteStorage,
        EscortLinkTokenStorage $escortLinkTokenStorage,
        TimezoneReadStorage $timezoneReadStorage,
        AccountServiceProviderInterface $accountServiceProvider,
        CityReadStorage $cityReadStorage,
        int $expireInDays,
        EscortSponsorWriteStorage $escortSponsorWriteStorage,
        EscortSideWriteStorage $escortSideWriteStorage,
        EscortStickyWriteStorage $escortStickyWriteStorage
    ) {
        $this->escortReadStorage = $escortReadStorage;
        $this->escortWriteStorage = $escortWriteStorage;
        $this->escortLinkTokenStorage = $escortLinkTokenStorage;
        $this->timezoneReadStorage = $timezoneReadStorage;
        $this->accountServiceProvider = $accountServiceProvider;
        $this->cityReadStorage = $cityReadStorage;
        $this->expireInDays = $expireInDays;
        $this->escortSponsorWriteStorage = $escortSponsorWriteStorage;
        $this->escortSideWriteStorage = $escortSideWriteStorage;
        $this->escortStickyWriteStorage = $escortStickyWriteStorage;
    }

    /**
     * @throws \Exception
     */
    public function add(CreateEscortCommand $command): CreateEscortResult
    {
        $stateIds = [];
        if (!empty($command->getLocationIds())) {
            $cities = $this->cityReadStorage->getByIds(...$command->getLocationIds());

            foreach ($cities as $city) {
                $stateIds[$city->getId()] = $city->getStateId();
            }
        }

        $escort = new Escort(
            $command->getAccountId(),
            $command->getType(),
            $command->getLocationIds(),
            $stateIds,
            $command->getFirstName(),
            $command->getMiddleName(),
            $command->getLastName(),
            $command->getTitle(),
            $command->getContent(),
            $command->getIsAvailableForMen(),
            $command->getIsAvailableForWomen(),
            $command->getIsAvailableForCouple(),
            $command->getIsAvailableForGroup(),
            $command->getIsAvailableForBlack(),
            $command->getIsIncallEnabled(),
            $command->getIncallRateHalfHour(),
            $command->getIncallRateOneHour(),
            $command->getIncallRateTwoHours(),
            $command->getIncallRateOvernight(),
            $command->getIsOutcallEnabled(),
            $command->getOutcallRateHalfHour(),
            $command->getOutcallRateOneHour(),
            $command->getOutcallRateTwoHours(),
            $command->getOutcallRateOvernight(),
            $command->getIsVisiting(),
            $command->getVisitingFrom(),
            $command->getVisitingTo(),
            $command->getPlaceDescription(),
            $command->getGfe(),
            $command->getGfeLimited(),
            $command->getTantra(),
            $command->getFetishDominant(),
            $command->getFetishSubmissive(),
            $command->getFetishSwitch(),
            $command->getFetish(),
            $command->getPhone(),
            $command->getEmail(),
            $command->getEmailVisibility(),
            $command->getWebsite(),
            $command->getFacebook(),
            $command->getTwitter(),
            $command->getTelegram(),
            $command->getGoogle(),
            $command->getInstagram(),
            $command->getWechat(),
            $command->getWhatsapp(),
            $command->getAge(),
            $command->getEthnicity(),
            $command->getLanguageIds(),
            $command->getHeightFeet(),
            $command->getHeightInches(),
            $command->getWeight(),
            $command->getHairColor(),
            $command->getEyeColor(),
            $command->getBodyType(),
            $command->getBust(),
            $command->getWaist(),
            $command->getHip(),
            $command->getCupSize(),
            $command->getPenisSize(),
            $command->getKitty(),
            $command->getIsPornstar(),
            $command->getIsPregnant(),
            $command->getTerId(),
            $command->getIsVisaAccepted(),
            $command->getIsAmexAccepted(),
            $command->getIsDiscoverAccepted(),
            $command->getImages(),
            $command->getVideos(),
            0,
            false,
            null,
            null,
            false
        );

        $escort->attachThumbnail($command->getImages()[0]);

        $this->escortWriteStorage->add($escort);

        if (!empty($command->getCityThumbnailLocation())) {
            foreach ($command->getCityThumbnailLocation() as $location) {
                $cityThumbnail = new EscortSponsor(
                    $location,
                    0,
                    $escort->getId(),
                    0,
                    $escort->getType()
                );

                $this->escortSponsorWriteStorage->add($cityThumbnail);

                $cityThumbnail->setProcessOfCreationStatus();
            }
        }

        if (!empty($command->getSideSponsorLocation())) {
            foreach ($command->getSideSponsorLocation() as $location) {
                $sideSponsor = new EscortSide(
                    $location,
                    0,
                    $escort->getId(),
                    0,
                    $escort->getType()
                );

                $this->escortSideWriteStorage->add($sideSponsor);

                $sideSponsor->setProcessOfCreationStatus();
            }
        }

        if (!empty($command->getStickySponsorWeek())) {
            foreach ($command->getStickySponsorWeek() as $location) {
                $sticky = new EscortSticky(
                  $escort->getId(),
                  $location,
                  $escort->getType(),
                  new Period(7)
                );

                $sticky->setProcessOfCreationStatus();

                $this->escortStickyWriteStorage->add($sticky);
            }
        }

        if (!empty($command->getStickySponsorMoth())) {
            foreach ($command->getStickySponsorMoth() as $location) {
                $sticky = new EscortSticky(
                    $escort->getId(),
                    $location,
                    $escort->getType(),
                    new Period(30)
                );

                $sticky->setProcessOfCreationStatus();

                $this->escortStickyWriteStorage->add($sticky);
            }
        }

        if ($command->getAccountId() === null) {
            $token = Uuid::uuid4();

            $this->escortLinkTokenStorage->add(
                new EscortLinkToken($escort->getId(), $token)
            );

            return new CreateEscortResult($escort->getId(), $token);
        }

        return new CreateEscortResult($escort->getId());
    }

    /**
     * @throws EscortNotFound
     * @throws AccessForbidden
     * @throws \Exception
     */
    public function update(UpdateEscortCommand $command): void
    {
        $escort = $this->getAndLock($command->getId());

        $escort->update(
            $command->getLocationIds(),
            $command->getStateIds(),
            $command->getFirstName(),
            $command->getMiddleName(),
            $command->getLastName(),
            $command->getTitle(),
            $command->getContent(),
            $command->getIsAvailableForMen(),
            $command->getIsAvailableForWomen(),
            $command->getIsAvailableForCouple(),
            $command->getIsAvailableForGroup(),
            $command->getIsAvailableForBlack(),
            $command->getIsIncallEnabled(),
            $command->getIncallRateHalfHour(),
            $command->getIncallRateOneHour(),
            $command->getIncallRateTwoHours(),
            $command->getIncallRateOvernight(),
            $command->getIsOutcallEnabled(),
            $command->getOutcallRateHalfHour(),
            $command->getOutcallRateOneHour(),
            $command->getOutcallRateTwoHours(),
            $command->getOutcallRateOvernight(),
            $command->getIsVisiting(),
            $command->getVisitingFrom(),
            $command->getVisitingTo(),
            $command->getPlaceDescription(),
            $command->getGfe(),
            $command->getGfeLimited(),
            $command->getTantra(),
            $command->getFetishDominant(),
            $command->getFetishSubmissive(),
            $command->getFetishSwitch(),
            $command->getFetish(),
            $command->getPhone(),
            $command->getEmail(),
            $command->getEmailVisibility(),
            $command->getWebsite(),
            $command->getFacebook(),
            $command->getTwitter(),
            $command->getTelegram(),
            $command->getGoogle(),
            $command->getInstagram(),
            $command->getWechat(),
            $command->getWhatsapp(),
            $command->getAge(),
            $command->getEthnicity(),
            $command->getLanguageIds(),
            $command->getHeightFeet(),
            $command->getHeightInches(),
            $command->getWeight(),
            $command->getHairColor(),
            $command->getEyeColor(),
            $command->getBodyType(),
            $command->getBust(),
            $command->getWaist(),
            $command->getHip(),
            $command->getCupSize(),
            $command->getPenisSize(),
            $command->getKitty(),
            $command->getIsPornstar(),
            $command->getIsPregnant(),
            $command->getTerId(),
            $command->getIsVisaAccepted(),
            $command->getIsAmexAccepted(),
            $command->getIsDiscoverAccepted(),
            $command->getImages(),
            $command->getVideos(),
            0,
            false,
            null,
            null,
            false
        );

        $escort->attachThumbnail($command->getImages()[0]);
    }

    public function adminUpdate(UpdateEscortAdminCommand $command): void
    {
        $this->getAndLock($command->getId())->adminUpdate($command);
    }

    /**
     * @throws EscortNotFound
     */
    public function sendEmail(int $escortId, Email $replyTo, string $message): void
    {
        PublisherContainer::instance()->publish(
            new EmailToEscortSent(
                new Email($this->get($escortId)->getEmail()),
                $replyTo,
                $message
            )
        );
    }

    /**
     * @throws AlreadyHasAccount
     * @throws EscortNotFound
     */
    public function linkToAccount(int $accountId, int $escortId): void
    {
        $escort = $this->getAndLock($escortId);

        if ($escort->getAccountId() !== null) {
            throw new AlreadyHasAccount();
        }

        $escort->setAccountId($accountId);
    }

    /**
     * @throws EscortNotFound
     * @throws \App\Security\Domain\NotEnoughTopUps
     */
    public function moveEscortToTop(int $escortId): void
    {
        $escort = $this->getAndLock($escortId);
        $escort->activate();

        foreach ($escort->getLocations() as $location) {
            $location->activate();
            $this->accountServiceProvider->removeTopUps(
                $escort->getAccountId(),
                1
            );
        }
    }

    /**
     * @throws EscortNotFound
     */
    public function activate(int $escortId): void
    {
        $escort = $this->get($escortId);
        $escort->activate();
        $escort->activateLocations();
    }

    /**
     * @throws EscortNotFound
     */
    public function removeEscort(int $id): void
    {
        $this->getAndLock($id)->delete();
    }

    public function changeLocation(int $escortId, int $fromLocationId, int $toLocationId): void
    {
        $city = $this->cityReadStorage->getById($toLocationId);
        $this->getAndLock($escortId)->changeLocation($fromLocationId, $city);
    }

    /**
     * @param string[] $images
     * @throws EscortNotFound
     * @throws \App\Escorts\Domain\EmptyImages
     */
    public function removeImages(int $id, array $images): void
    {
        $this->getAndLock($id)->removeImages($images);
    }

    /**
     * @param string[] $videos
     * @throws EscortNotFound
     * @throws \App\Escorts\Domain\EmptyVideos
     */
    public function removeVideos(int $id, array $videos): void
    {
        $this->getAndLock($id)->removeVideos($videos);
    }

    /**
     * @throws EscortNotFound
     */
    public function setThumbnail(int $id, string $thumbnailName): void
    {
        $this->getAndLock($id)->attachThumbnail($thumbnailName);
    }

    /**
     * @throws EscortNotFound
     */
    private function getAndLock(int $id): Escort
    {
        $escort = $this->escortWriteStorage->getAndLock($id);

        if ($escort === null) {
            throw new EscortNotFound();
        }

        return $escort;
    }

    private function get(int $id): Escort
    {
        $escort = $this->escortReadStorage->get($id);

        if ($escort === null) {
            throw new EscortNotFound();
        }

        return $escort;
    }

    /**
     * @throws EscortNotFound
     */
    public function duplicate(int $id, int $escortType): CreateEscortResult
    {
        $escort = $this->get($id);
        $duplicate = clone $escort;

        $duplicate->setType(EscortType::getValueOf($escortType));

        foreach ($escort->getImages() as $image) {
            $duplicate->addImage(new EscortImage($duplicate, $image->getFilename(), $image->getUploadedAt()));
        }

        foreach ($escort->getVideos() as $video) {
            $duplicate->addVideo(
                new EscortVideo(
                    $video->getAccountId(),
                    $duplicate,
                    $video->getFilename(),
                    $video->getThumbnail(),
                    $video->getWidth(),
                    $video->getHeight(),
                )
            );
        }

        foreach ($escort->getLocations() as $location) {
            $duplicate->addLocation(
                new EscortLocation(
                    $duplicate,
                    $location->getStateId(),
                    $location->getLocationId(),
                    $location->getType(),
                    new \DateTimeImmutable(),
                    Status::processOfCreation()
                )
            );
        }

        $duplicate->setInActive();

        $this->escortWriteStorage->add($duplicate);

        return new CreateEscortResult($duplicate->getId());
    }

    public function getRecurringPeriod(): int
    {
        return $this->expireInDays;
    }
}
