<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class Period
{
    /**
     * @ORM\Column(name="days", type="integer", length=11, nullable=true)
     */
    private ?int $inDays;

    /**
     * @ORM\Column(name="created_stamp", type="timestamp", nullable=true)
     */
    private ?\DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(name="expire_stamp", type="timestamp", nullable=true)
     */
    private ?\DateTimeImmutable $expireAt;

    public function __construct(int $inDays)
    {
        $this->inDays = $inDays;
        $this->createdAt = new \DateTimeImmutable();
        $this->expireAt = new \DateTimeImmutable('+' . ($inDays + 1) . ' days');
    }

    public function setDaysInExpireAt(int $inDays): void
    {
        if ($this->expireAt <= new \DateTimeImmutable()) {
            $this->expireAt = new \DateTimeImmutable('+' . ($inDays + 1) . ' days');
            return;
        }

        $this->expireAt->modify('+' . ($inDays + 1) . ' days');
    }

    public function isExpired(): bool
    {
        return $this->getExpireAt() < new \DateTimeImmutable();
    }

    public function getInDays(): ?int
    {
        return $this->inDays;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getExpireAt(): ?\DateTimeImmutable
    {
        return $this->expireAt;
    }
}
