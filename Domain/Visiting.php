<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class Visiting
{
    /**
     * @ORM\Column(name="visiting", type="boolean")
     */
    private bool $isVisiting;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $visitingFrom;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $visitingTo;

    public function __construct(
        bool $isVisiting,
        ?\DateTimeImmutable $visitingFrom,
        ?\DateTimeImmutable $visitingTo
    ) {
        $this->isVisiting = $isVisiting;
        $this->visitingFrom = $visitingFrom;
        $this->visitingTo = $visitingTo;
    }

    public function isVisiting(): bool
    {
        return $this->isVisiting;
    }

    public function getVisitingFrom(): ?\DateTimeImmutable
    {
        if (!$this->isVisiting()) {
            return null;
        }

        return $this->visitingFrom;
    }

    public function getVisitingTo(): ?\DateTimeImmutable
    {
        if (!$this->isVisiting()) {
            return null;
        }

        return $this->visitingTo;
    }
}
