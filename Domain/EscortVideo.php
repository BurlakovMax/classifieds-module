<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="classified_video")
 */
class EscortVideo
{
    /**
     * @ORM\Column(name="id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $accountId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Escorts\Domain\Escort")
     * @ORM\JoinColumn(name="classified_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private Escort $escort;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $filename;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $thumbnail;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $width;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $height;

    /**
     * @ORM\Column(name="converted", type="video_converted_status_type")
     */
    private VideoConvertedStatus $videoConvertedStatus;

    /**
     * @ORM\Column(name="created_stamp", type="timestamp")
     */
    private \DateTimeImmutable $createdAt;

    public function __construct(
        int $accountId,
        Escort $escort,
        string $filename,
        ?string $thumbnail,
        ?int $width,
        ?int $height
    ) {
        $this->id = 0;
        $this->accountId = $accountId;
        $this->escort = $escort;
        $this->filename = $filename;
        $this->thumbnail = $thumbnail;
        $this->width = $width;
        $this->height = $height;
        $this->videoConvertedStatus = VideoConvertedStatus::successfully();
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function getVideoConvertedStatus(): VideoConvertedStatus
    {
        return $this->videoConvertedStatus;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }
}