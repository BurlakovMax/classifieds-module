<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\EmailGate\Domain\EmailData;

interface EmailProvider
{
    /**
     * @throws \Throwable
     */
    public function sendSync(EmailData $emailData): void;
}
