<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FileUpload
{
    public function save(string $name, UploadedFile $file): string;
}