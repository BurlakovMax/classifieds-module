<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

interface EscortSideWriteStorage
{
    public function add(EscortSide $escortSide): void;

    public function getAndLockByEscortIdAndLocationId(int $escortId, int $locationId): ?EscortSide;
}