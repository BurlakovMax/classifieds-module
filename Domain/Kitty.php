<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use LitGroup\Enumerable\Enumerable;

final class Kitty extends Enumerable
{
    public static function bald(): self
    {
        return self::createEnum(1);
    }

    public static function partiallyShaved(): self
    {
        return self::createEnum(2);
    }

    public static function trimmed(): self
    {
        return self::createEnum(3);
    }

    public static function natural(): self
    {
        return self::createEnum(4);
    }

    /**
     * @deprecated For legacy support only
     */
    public static function none(): self
    {
        return self::createEnum(0);
    }

    public function __toString(): string
    {
        return (string) $this->getRawValue();
    }
}
