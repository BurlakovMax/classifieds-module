<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Ramsey\Uuid\UuidInterface;

final class CreateEscortResult
{
    private int $id;

    private ?UuidInterface $linkToken;

    public function __construct(int $id, ?UuidInterface $linkToken = null)
    {
        $this->id = $id;
        $this->linkToken = $linkToken;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLinkToken(): ?UuidInterface
    {
        return $this->linkToken;
    }
}
