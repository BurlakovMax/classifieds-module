<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Escorts\Infrastructure\Persistence\EscortStickyDoctrineRepository")
 * @ORM\Table(name="classified_sticky")
 */
class EscortSticky
{
    /**
     * @ORM\Column(name="id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="classified_id", type="integer", length=11)
     */
    private int $escortId;

    /**
     * @ORM\Column(name="loc_id", type="integer", length=1)
     */
    private int $locationId;

    /**
     * @ORM\Column(type="escort_type", nullable=true)
     */
    private ?EscortType $type;

    /**
     * @ORM\Column(type="integer", length=1, nullable=true)
     */
    private ?int $position;

    /**
     * @var Period
     *
     * @ORM\Embedded(class="App\Escorts\Domain\Period", columnPrefix=false)
     */
    private $period;

    /**
     * @ORM\Column(name="done", type="status_type")
     */
    private Status $status;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $notified;

    public function __construct(
        int $escortId,
        int $locationId,
        EscortType $type,
        Period $period
    ) {
        $this->id = 0;
        $this->escortId = $escortId;
        $this->locationId = $locationId;
        $this->type = $type;
        $this->position = null;
        $this->period = $period;
        $this->status = Status::active();;
        $this->notified = false;
    }

    public function setProcessOfCreationStatus(): void
    {
        $this->status = Status::processOfCreation();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEscortId(): int
    {
        return $this->escortId;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getType(): ?EscortType
    {
        return $this->type;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function getInDays(): ?int
    {
        return $this->getPeriod()->getInDays();
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->getPeriod()->getCreatedAt();
    }

    public function getExpireAt(): ?\DateTimeImmutable
    {
        return $this->getPeriod()->getExpireAt();
    }

    public function isExpired(): bool
    {
        return $this->getPeriod()->isExpired();
    }

    public function setDaysInExpireAt(int $inDays): void
    {
        $this->getPeriod()->setDaysInExpireAt($inDays);
    }

    private function getPeriod(): Period
    {
        return $this->period;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function getNotified(): bool
    {
        return $this->notified;
    }
}
