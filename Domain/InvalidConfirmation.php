<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use LazyLemurs\Exceptions\Exception;

final class InvalidConfirmation extends Exception
{

}
