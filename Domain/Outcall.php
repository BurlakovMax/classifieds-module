<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class Outcall
{
    /**
     * @ORM\Column(name="outcall", type="boolean")
     */
    private bool $isEnabled;

    /**
     * @ORM\Column(name="outcall_rate_hh", type="smallint", options={"default"=0})
     */
    private ?int $outcallRateHalfHour;

    /**
     * @ORM\Column(name="outcall_rate_h", type="smallint", options={"default"=0})
     */
    private ?int $outcallRateOneHour;

    /**
     * @ORM\Column(name="outcall_rate_2h", type="smallint", options={"default"=0})
     */
    private ?int $outcallRateTwoHours;

    /**
     * @ORM\Column(name="outcall_rate_day", type="smallint", options={"default"=0})
     */
    private ?int $outcallRateOvernight;

    /**
     * @ORM\Column(name="outcall_rate", type="smallint", nullable=true)
     */
    private ?int $outcallRate;

    public function __construct(
        bool $isEnabled,
        ?int $incallRateHalfHour,
        ?int $incallRateOneHour,
        ?int $incallRateTwoHours,
        ?int $incallRateOvernight
    ) {
        $this->isEnabled = $isEnabled;
        $this->outcallRateHalfHour = $incallRateHalfHour ?? 0;
        $this->outcallRateOneHour = $incallRateOneHour ?? 0;
        $this->outcallRateTwoHours = $incallRateTwoHours ?? 0;
        $this->outcallRateOvernight = $incallRateOvernight ?? 0;
        $this->outcallRate = null;
    }

    public function getIsEnabled(): bool
    {
        return $this->isEnabled;
    }

    public function getOutcallRateHalfHour(): int
    {
        return $this->outcallRateHalfHour;
    }

    public function getOutcallRateOneHour(): int
    {
        return $this->outcallRateOneHour;
    }

    public function getOutcallRateTwoHours(): int
    {
        return $this->outcallRateTwoHours;
    }

    public function getOutcallRateOvernight(): int
    {
        return $this->outcallRateOvernight;
    }

    public function getOutcallRate(): ?int
    {
        return $this->outcallRate;
    }
}
