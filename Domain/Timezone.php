<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Escorts\Infrastructure\Persistence\TimezoneDoctrineRepository")
 * @ORM\Table(name="timezone")
 */
class Timezone
{
    private const DEFAULT_TIME_ZONE = 'PST';

    /**
     * @ORM\Column(name="loc_id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $locationId;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $time;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private string $timezone;

    /**
     * @throws \Exception
     */
    public static function calculateRenewAt(int $autoRenewTime, int $autoRenewFrequency, ?string $timeZone): \DateTimeImmutable
    {
        $hours = floor($autoRenewTime / 60);
        $minutes = $autoRenewTime - ($hours * 60);

        $renewAt = new \DateTimeImmutable("tomorrow", new \DateTimeZone($timeZone ?? self::DEFAULT_TIME_ZONE));
        if ($autoRenewFrequency > 1) {
            $renewAt->modify('+' . ($autoRenewFrequency - 1) . ' days');
        }

        $renewAt->setTime($hours, $minutes);

        if ($renewAt < new \DateTimeImmutable()) {
            $renewAt = new \DateTimeImmutable('+1 day');
        }

        return $renewAt;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getTime(): int
    {
        return $this->time;
    }

    public function getTimezone(): string
    {
        return $this->timezone;
    }
}