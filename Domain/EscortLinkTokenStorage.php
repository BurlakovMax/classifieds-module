<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Ramsey\Uuid\UuidInterface;

interface EscortLinkTokenStorage
{
    public function get(int $escortId, UuidInterface $token): ?EscortLinkToken;

    public function add(EscortLinkToken $linkToken): void;

    public function delete(EscortLinkToken $linkToken): void;
}
