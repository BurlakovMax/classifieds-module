<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class Incall
{
    /**
     * @ORM\Column(name="incall", type="boolean")
     */
    private bool $isEnabled;

    /**
     * @ORM\Column(name="incall_rate_hh", type="smallint", options={"default"=0})
     */
    private ?int $incallRateHalfHour;

    /**
     * @ORM\Column(name="incall_rate_h", type="smallint", options={"default"=0})
     */
    private ?int $incallRateOneHour;

    /**
     * @ORM\Column(name="incall_rate_2h", type="smallint", options={"default"=0})
     */
    private ?int $incallRateTwoHours;

    /**
     * @ORM\Column(name="incall_rate_day", type="smallint", options={"default"=0})
     */
    private ?int $incallRateOvernight;

    /**
     * @ORM\Column(name="incall_rate", type="smallint", nullable=true)
     */
    private ?int $incallRate;

    public function __construct(
        bool $isEnabled,
        ?int $incallRateHalfHour,
        ?int $incallRateOneHour,
        ?int $incallRateTwoHours,
        ?int $incallRateOvernight
    ) {
        $this->isEnabled = $isEnabled;
        $this->incallRateHalfHour = $incallRateHalfHour ?? 0;
        $this->incallRateOneHour = $incallRateOneHour ?? 0;
        $this->incallRateTwoHours = $incallRateTwoHours ?? 0;
        $this->incallRateOvernight = $incallRateOvernight ?? 0;
        $this->incallRate = null;
    }

    public function getIsEnabled(): bool
    {
        return $this->isEnabled;
    }

    public function getIncallRateHalfHour(): ?int
    {
        return $this->incallRateHalfHour;
    }

    public function getIncallRateOneHour(): ?int
    {
        return $this->incallRateOneHour;
    }

    public function getIncallRateTwoHours(): ?int
    {
        return $this->incallRateTwoHours;
    }

    public function getIncallRateOvernight(): ?int
    {
        return $this->incallRateOvernight;
    }

    public function getIncallRate(): ?int
    {
        return $this->incallRate;
    }
}
