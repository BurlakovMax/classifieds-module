<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Escorts\Infrastructure\Persistence\EscortWaitingListDoctrineRepository")
 * @ORM\Table(name="classified_waiting_list")
 */
class EscortWaitingList
{
    /**
     * @ORM\Column(name="id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $accountId;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $locationId;

    /**
     * @ORM\Column(name="status_id", type="escort_waiting_list_type")
     */
    private StatusEscortWaitingList $status;

    /**
     * @ORM\Column(name="classified_type_id", type="escort_type")
     */
    private EscortType $type;

    /**
     * @ORM\Column(name="created_stamp", type="timestamp")
     */
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(name="updated_stamp", type="timestamp")
     */
    private \DateTimeImmutable $updatedAt;

    public function __construct(
        int $accountId,
        int $locationId,
        EscortType $type
    ) {
        $this->id = 0;
        $this->accountId = $accountId;
        $this->locationId = $locationId;
        $this->status = StatusEscortWaitingList::pending();
        $this->type = $type;
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function cancel()
    {
        $this->status = StatusEscortWaitingList::canceled();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getStatus(): StatusEscortWaitingList
    {
        return $this->status;
    }

    public function getType(): EscortType
    {
        return $this->type;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }
}