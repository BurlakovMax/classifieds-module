<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\Application\Search\SearchQuery;

interface PhoneBlacklistStorage
{
    public function getById(int $id): ?PhoneBlacklist;

    public function add(PhoneBlacklist $phone): void;

    public function remove(PhoneBlacklist $phone): void;

    /**
     * @return PhoneBlacklist[]
     */
    public function getListBySearchQuery(SearchQuery $query): array;

    public function getCountBySearchQuery(SearchQuery $query): int;
}
