<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\Application\Search\SearchQuery;

interface EscortSponsorReadStorage
{
    /**
     * @return EscortSponsor[]
     */
    public function getBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;

    public function getByEscortId(int $escortId): ?EscortSponsor;

    public function getByEscortIdAndLocationId(int $escortId, int $locationId): ?EscortSponsor;

    /**
     * @return int[]
     */
    public function getEscortIdsByLocationId(int $locationId): array;

    public function countActiveByLocationId(int $locationId): int;
}