<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="App\Escorts\Infrastructure\Persistence\EscortLinkTokenDoctrineRepository")
 * @ORM\Table(name="classifieds_link_token")
 */
class EscortLinkToken
{
    /**
     * @ORM\Column(name="classified_id", type="integer")
     * @ORM\Id
     */
    private int $escortId;

    /**
     * @ORM\Column(name="token", type="uuid_binary")
     * @ORM\Id
     */
    private UuidInterface $token;

    public function __construct(int $escortId, UuidInterface $token)
    {
        $this->escortId = $escortId;
        $this->token = $token;
    }
}
