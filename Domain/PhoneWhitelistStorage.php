<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\Application\Search\SearchQuery;

interface PhoneWhitelistStorage
{
    public function add(PhoneWhitelist $phone): void;

    /**
     * @return PhoneWhitelist[]
     */
    public function getListBySearchQuery(SearchQuery $query): array;

    public function getCountBySearchQuery(SearchQuery $query): int;
}
