<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use LazyLemurs\Exceptions\NotFoundException;

final class PhoneNotInBlacklist extends NotFoundException
{

}
