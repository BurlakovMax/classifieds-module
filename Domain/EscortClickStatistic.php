<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Escorts\Infrastructure\Persistence\EscortClickStatisticDoctrineRepository")
 * @ORM\Table(name="classifieds_click")
 */
class EscortClickStatistic
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="post_id", type="integer", length=11)
     */
    private int $escortId;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="loc_id", type="integer", length=11)
     */
    private int $locationId;

    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private string $date;

    /**
     * @ORM\Column(name="c", type="integer", length=11, options={"default"=1})
     */
    private int $clickCount;

    public function __construct(int $escortId, int $locationId, string $date)
    {
        $this->escortId = $escortId;
        $this->locationId = $locationId;
        $this->date = $date;
        $this->clickCount = 1;
    }

    public function addClick(): void
    {
        $this->clickCount += 1;
    }

    public function getEscortId(): int
    {
        return $this->escortId;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getClickCount(): int
    {
        return $this->clickCount;
    }
}
