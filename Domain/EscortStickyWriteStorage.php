<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

interface EscortStickyWriteStorage
{
    public function add(EscortSticky $escortSticky): void;

    public function getAndLock(int $id): ?EscortSticky;
}