<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

interface TimezoneReadStorage
{
    public function get(int $locationId): ?Timezone;
}