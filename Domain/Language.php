<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\DomainSupport\Enumerable;

final class Language extends Enumerable
{
    public static function albanian(): self
    {
        return self::createEnum(1);
    }

    public static function arabic(): self
    {
        return self::createEnum(2);
    }

    public static function armenian(): self
    {
        return self::createEnum(3);
    }

    public static function belarusian(): self
    {
        return self::createEnum(4);
    }

    public static function bosnian(): self
    {
        return self::createEnum(5);
    }

    public static function celtic(): self
    {
        return self::createEnum(6);
    }

    public static function chineseSimplified(): self
    {
        return self::createEnum(7);
    }

    public static function chineseTraditional(): self
    {
        return self::createEnum(8);
    }

    public static function croatian(): self
    {
        return self::createEnum(9);
    }

    public static function czech(): self
    {
        return self::createEnum(10);
    }

    public static function danish(): self
    {
        return self::createEnum(11);
    }

    public static function dutch(): self
    {
        return self::createEnum(12);
    }

    public static function english(): self
    {
        return self::createEnum(13);
    }

    public static function finnish(): self
    {
        return self::createEnum(14);
    }

    public static function french(): self
    {
        return self::createEnum(15);
    }

    public static function german(): self
    {
        return self::createEnum(16);
    }

    public static function greek(): self
    {
        return self::createEnum(17);
    }

    public static function italian(): self
    {
        return self::createEnum(18);
    }

    public static function japanese(): self
    {
        return self::createEnum(19);
    }

    public static function korean(): self
    {
        return self::createEnum(20);
    }

    public static function lao(): self
    {
        return self::createEnum(21);
    }

    public static function latvian(): self
    {
        return self::createEnum(22);
    }

    public static function lithuanian(): self
    {
        return self::createEnum(23);
    }

    public static function persian(): self
    {
        return self::createEnum(24);
    }

    public static function polish(): self
    {
        return self::createEnum(25);
    }

    public static function portuguese(): self
    {
        return self::createEnum(26);
    }

    public static function norwegian(): self
    {
        return self::createEnum(27);
    }

    public static function russian(): self
    {
        return self::createEnum(28);
    }

    public static function serbian(): self
    {
        return self::createEnum(29);
    }

    public static function spanish(): self
    {
        return self::createEnum(30);
    }

    public static function swedish(): self
    {
        return self::createEnum(31);
    }

    public static function thai(): self
    {
        return self::createEnum(32);
    }

    public static function turkish(): self
    {
        return self::createEnum(33);
    }

    public static function ukranian(): self
    {
        return self::createEnum(34);
    }

    public static function vietnamese(): self
    {
        return self::createEnum(35);
    }

    public static function emptyValue(): self
    {
        return self::createEnum('');
    }
}
