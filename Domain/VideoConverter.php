<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

interface VideoConverter
{
    public function convert(string $name, string $srcPathToUploadedFile, string $saveToPath): void;
}