<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="classifieds_image")
 */
class EscortImage
{
    /**
     * @ORM\Column(name="image_id", type="integer", length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Escorts\Domain\Escort")
     * @ORM\JoinColumn(name="id", referencedColumnName="id", onDelete="CASCADE")
     */
    private Escort $escort;

    /**
     * @ORM\Column(name="filename", type="string", length=255)
     */
    private string $filename;

    /**
     * @ORM\Column(name="uploaded_stamp", type="timestamp", nullable=true)
     */
    private ?\DateTimeImmutable $uploadedAt;

    public function __construct(
        Escort $escort,
        string $filename,
        \DateTimeImmutable $uploadedAt
    ) {
        $this->id = 0;
        $this->escort = $escort;
        $this->filename = $filename;
        $this->uploadedAt = $uploadedAt;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getUploadedAt(): ?\DateTimeImmutable
    {
        return $this->uploadedAt;
    }
}
