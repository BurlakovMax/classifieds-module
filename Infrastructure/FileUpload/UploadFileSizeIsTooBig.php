<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\FileUpload;

use LazyLemurs\Exceptions\Exception;

final class UploadFileSizeIsTooBig extends Exception
{

}