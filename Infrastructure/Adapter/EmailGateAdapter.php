<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Adapter;

use App\EmailGate\Domain\EmailData;
use App\EmailGate\Domain\MessageSender;
use App\Escorts\Domain\EmailProvider;

final class EmailGateAdapter implements EmailProvider
{
    private MessageSender $messageSender;

    public function __construct(MessageSender $messageSender)
    {
        $this->messageSender = $messageSender;
    }

    public function sendSync(EmailData $emailData): void
    {
        $this->messageSender->sendSync($emailData);
    }
}
