<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Adapter;

use App\Escorts\Domain\Account;
use App\Escorts\Domain\AccountReadStorage;
use App\Security\Application\AccountQueryProcessor as QueryProcessor;

final class AccountQueryProcessor implements AccountReadStorage
{
    private QueryProcessor $queryProcessor;

    public function __construct(QueryProcessor $queryProcessor)
    {
        $this->queryProcessor = $queryProcessor;
    }

    /**
     * @throws \App\Security\Domain\AccountNotFound
     */
    public function getAccount(int $accountId): Account
    {
        $account = $this->queryProcessor->getAccount($accountId);

        return new Account(
            $account->getId(),
            $account->getRoles(),
            $account->getEmail(),
            $account->getName(),
            $account->getForumPostCount(),
            $account->isWhitelisted(),
            $account->getAvatar(),
            $account->getTopUps()
        );
    }
}