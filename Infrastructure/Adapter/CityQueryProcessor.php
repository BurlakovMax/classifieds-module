<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Adapter;

use App\Escorts\Domain\City;
use App\Escorts\Domain\CityReadStorage;
use App\Locations\Application\CityData;
use App\Locations\Application\CityQueryProcessor as QueryProcessor;

final class CityQueryProcessor implements CityReadStorage
{
    private QueryProcessor $queryProcessor;

    public function __construct(QueryProcessor $queryProcessor)
    {
        $this->queryProcessor = $queryProcessor;
    }

    /**
     * @throws \App\Locations\Application\CityNotFound
     */
    public function getById(int $id): City
    {
        $city = $this->queryProcessor->getById($id);

        return $this->mapToData($city);
    }

    /**
     * @param int ...$ids
     * @return City[]
     */
    public function getByIds(int ...$ids): array
    {
        return array_map([$this, 'mapToData'], $this->queryProcessor->getByIds(...$ids));
    }

    private function mapToData(CityData $city): City
    {
        return new City(
            $city->getId(),
            $city->getName(),
            $city->getUrl(),
            $city->getDescription(),
            $city->getImages(),
            $city->getVideo(),
            $city->getActive(),
            $city->getIsSignificant(),
            $city->getCountryId(),
            $city->getStateId(),
            $city->getStateName(),
            $city->getCountryName()
        );
    }
}