<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Adapter;

use App\Escorts\Domain\AccountServiceProviderInterface;
use App\Security\Domain\AccountNotFound;
use App\Security\Domain\AccountService;

final class AccountServiceProvider implements AccountServiceProviderInterface
{
    private AccountService $accountService;

    public function __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    /**
     * @throws AccountNotFound
     * @throws \App\Security\Domain\NotEnoughTopUps
     */
    public function removeTopUps(int $accountId, int $topUps = 1): void
    {
       $this->accountService->removeTopUps($accountId, $topUps);
    }
}
