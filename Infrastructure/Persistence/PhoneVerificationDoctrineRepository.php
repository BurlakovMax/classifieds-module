<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence;

use App\Escorts\Domain\PhoneVerification;
use App\Escorts\Domain\PhoneVerificationStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use LazyLemurs\Structures\PhoneNumber;

final class PhoneVerificationDoctrineRepository extends EntityRepository implements PhoneVerificationStorage
{
    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function get(PhoneNumber $phone, ?int $accountId, ?int $escortId): ?PhoneVerification
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(
                            $accountId !== null
                                ? Criteria::expr()->eq('accountId', $accountId)
                                : Criteria::expr()->eq('escortId', $escortId)
                        )
                        ->andWhere(Criteria::expr()->eq('phone', $phone->getValue()))
                )
                ->getQuery()
                ->getOneOrNullResult()
            ;
    }

    /**
     * @return PhoneVerification[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getByPhoneNumberAndVerified(PhoneNumber $phoneNumber): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('phone', $phoneNumber->getValue()))
                        ->andWhere(Criteria::expr()->eq('isVerified', true))
                )
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getAndLock(PhoneNumber $phone, ?int $accountId, ?int $escortId): ?PhoneVerification
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(
                            $accountId !== null
                                ? Criteria::expr()->eq('accountId', $accountId)
                                : Criteria::expr()->eq('escortId', $escortId)
                        )
                        ->andWhere(Criteria::expr()->eq('phone', $phone->getValue()))
                )
                ->getQuery()
                ->setLockMode(LockMode::PESSIMISTIC_WRITE)
                ->getOneOrNullResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(PhoneVerification $phoneVerification): void
    {
        $this->getEntityManager()->persist($phoneVerification);
    }
}
