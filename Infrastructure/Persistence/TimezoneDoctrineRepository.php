<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence;

use App\Escorts\Domain\Timezone;
use App\Escorts\Domain\TimezoneReadStorage;
use Doctrine\ORM\EntityRepository;

final class TimezoneDoctrineRepository extends EntityRepository implements TimezoneReadStorage
{
    public function get(int $locationId): ?Timezone
    {
        return $this->find($locationId);
    }
}