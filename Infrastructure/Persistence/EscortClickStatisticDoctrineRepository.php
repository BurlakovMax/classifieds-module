<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence;

use App\Escorts\Domain\EscortClickStatistic;
use App\Escorts\Domain\EscortClickStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;

final class EscortClickStatisticDoctrineRepository extends EntityRepository implements EscortClickStorage
{
    /**
     * @return EscortClickStatistic[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getByEscortId(int $escortId, \DateTimeImmutable $dateFrom, \DateTimeImmutable $dateTo): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('t.escortId', $escortId))
                        ->andWhere(
                            Criteria::expr()->orX(
                                Criteria::expr()->gte('t.date', $dateFrom),
                                Criteria::expr()->lte('t.date', $dateTo)
                            )
                        )
                )
                ->getQuery()
                ->getResult();
    }


    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getAndLockById(int $escortId, int $locationId, string $date): ?EscortClickStatistic
    {
       return $this->createQueryBuilder('t')
            ->addCriteria(
                Criteria::create()
                    ->andWhere(Criteria::expr()->eq('t.escortId', $escortId))
                    ->andWhere(Criteria::expr()->eq('t.locationId', $locationId))
                    ->andWhere(Criteria::expr()->eq('t.date', $date))
            )
            ->getQuery()
            ->setLockMode(LockMode::PESSIMISTIC_WRITE)
            ->getOneOrNullResult();
    }

    public function add(EscortClickStatistic $statistic): void
    {
        $this->getEntityManager()->persist($statistic);
    }
}
