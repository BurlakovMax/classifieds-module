<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Escorts\Domain\EscortSide;
use App\Escorts\Domain\EscortSideReadStorage;
use App\Escorts\Domain\EscortSideWriteStorage;
use App\Escorts\Domain\EscortType;
use App\Escorts\Domain\Status;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;

final class EscortSideDoctrineRepository extends EntityRepository implements EscortSideReadStorage, EscortSideWriteStorage
{
    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(EscortSide $escortSide): void
    {
        $this->getEntityManager()->persist($escortSide);
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getAndLockByEscortIdAndLocationId(int $escortId, int $locationId): ?EscortSide
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('escortId', $escortId))
                        ->andWhere(Criteria::expr()->eq('locationId', $locationId))
                        ->andWhere(
                            Criteria::expr()->in(
                                't.status',
                                [
                                    Status::active(),
                                    Status::invisible(),
                                    Status::processOfCreation(),
                                ]
                            )
                        )
                )
                ->getQuery()
                ->setLockMode(LockMode::PESSIMISTIC_WRITE)
                ->getOneOrNullResult()
            ;
    }

    /**
     * @return EscortSide[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $queryBuilder =
            $this
                ->createQueryBuilder('t')
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->addCriteria(
                    Limitation::limitation($query->getLimitation())
                )
        ;

        Ordering::add($queryBuilder, $query->getOrderQueries());

        return
            $queryBuilder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getByEscortIdAndLocationId(int $escortId, int $locationId): ?EscortSide
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('escortId', $escortId))
                        ->andWhere(Criteria::expr()->eq('locationId', $locationId))
                )
                ->getQuery()
                ->getOneOrNullResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getByEscortId(int $escortId): ?EscortSide
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('escortId', $escortId))
                        ->andWhere(Criteria::expr()->gte('status', Status::active()))
                )
                ->getQuery()
                ->getOneOrNullResult()
            ;
    }

    /**
     * @return int[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getEscortIdsByTypeAndLocationId(EscortType $type, int $locationId): array
    {
        return $this->createQueryBuilder('t')
            ->select('t.escortId')
            ->addCriteria(
                Criteria::create()
                    ->andWhere(Criteria::expr()->eq('locationId', $locationId))
                    ->andWhere(Criteria::expr()->eq('type', $type))
            )
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countByTypeAndLocationId(EscortType $type, int $locationId): int
    {
        $count =
            $this->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('locationId', $locationId))
                        ->andWhere(Criteria::expr()->eq('type', $type))
                        ->andWhere(Criteria::expr()->eq('status', Status::active()))
                )
                ->getQuery()
                ->getOneOrNullResult()
            ;

        return (int) $count['count'];
    }
}