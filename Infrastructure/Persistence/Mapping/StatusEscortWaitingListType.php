<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence\Mapping;

use App\Escorts\Domain\StatusEscortWaitingList;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class StatusEscortWaitingListType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return StatusEscortWaitingList::class;
    }

    public function getName()
    {
        return 'escort_waiting_list_type';
    }
}