<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence\Mapping;

use App\Escorts\Domain\Kitty;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class KittyType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return Kitty::class;
    }

    public function getName(): string
    {
        return 'kitty';
    }
}
