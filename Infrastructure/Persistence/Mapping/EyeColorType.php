<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence\Mapping;

use App\Escorts\Domain\EyeColor;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class EyeColorType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return EyeColor::class;
    }

    public function getName()
    {
        return 'eye_color';
    }
}