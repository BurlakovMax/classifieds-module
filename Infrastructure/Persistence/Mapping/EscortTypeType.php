<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence\Mapping;

use App\Escorts\Domain\EscortType;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class EscortTypeType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return EscortType::class;
    }

    public function getName()
    {
        return 'escort_type';
    }
}