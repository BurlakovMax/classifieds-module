<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence\Mapping;

use App\Escorts\Domain\Status;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class StatusType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return Status::class;
    }

    public function getName()
    {
        return 'status_type';
    }
}