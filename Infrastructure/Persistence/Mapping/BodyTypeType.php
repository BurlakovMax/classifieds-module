<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence\Mapping;

use App\Escorts\Domain\BodyType;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class BodyTypeType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return BodyType::class;
    }

    public function getName(): string
    {
        return 'body_type';
    }
}
