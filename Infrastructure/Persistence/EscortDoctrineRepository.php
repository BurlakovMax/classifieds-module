<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Escorts\Domain\Escort;
use App\Escorts\Domain\EscortReadStorage;
use App\Escorts\Domain\EscortWriteStorage;
use App\Escorts\Domain\Status;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

final class EscortDoctrineRepository extends EntityRepository implements EscortReadStorage, EscortWriteStorage
{
    public function get(int $id): ?Escort
    {
        return $this->find($id);
    }

    public function getAndLock(int $id): ?Escort
    {
        return $this->find($id, LockMode::PESSIMISTIC_WRITE);
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getActiveByIdAndAccountId(int $id, int $accountId): ?Escort
    {
        return
            $this->createActiveQuery()
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('t.id', $id))
                        ->andWhere(Criteria::expr()->eq('t.accountId', $accountId))
                )
                ->getQuery()
                ->getOneOrNullResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getLastActiveByAccountId(int $accountId): ?Escort
    {
        return
            $this->createActiveQuery()
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('t.accountId', $accountId))
                        ->orderBy(['t.id' => Criteria::DESC])
                        ->setMaxResults(1)
                )
                ->getQuery()
                ->getOneOrNullResult()
            ;
    }

    /**
     * @return int[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getIdsBySearchQuery(SearchQuery $query): array
    {
        $queryBuilder =
            $this
                ->createQueryBuilder('t')
                ->select('t.id')
                ->innerJoin('t.locations', 'locations')
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->addCriteria(
                    Limitation::limitation($query->getLimitation())
                )
        ;

        Ordering::add($queryBuilder, $query->getOrderQueries());

        return
            $queryBuilder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countIdsBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->innerJoin('t.locations', 'locations')
                ->select('COUNT(t.id) as count')
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @return Escort[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $queryBuilder =
            $this
                ->createQueryBuilder('t')
                ->innerJoin('t.locations', 'locations')
                ->addCriteria(
                    FuzzySearchBuilder::search(
                        $query->getFuzzy(),
                        [
                            't.phone',
                            't.email',
                            't.title',
                            't.content',
                        ]
                    )
                )
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->addCriteria(
                    Limitation::limitation($query->getLimitation())
                )
            ;

        Ordering::add($queryBuilder, $query->getOrderQueries());

        return
            $queryBuilder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->innerJoin('t.locations', 'locations')
                ->select('COUNT(t.id) as count')
                ->addCriteria(
                    FuzzySearchBuilder::search(
                        $query->getFuzzy(),
                        [
                            't.phone',
                            't.email',
                            't.title',
                            't.content',
                        ]
                    )
                )
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @param int[] $ids
     * @return Escort[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getByIds(array $ids): array
    {
        return
            $this->createActiveQuery()
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->in('id', $ids))
                )
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @param FilterQuery[] $filters
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countByFilterQuery(array $filters): int
    {
        $count =
            $this->createActiveQuery()
                ->addCriteria(CriteriaBuilder::createByFilters($filters))
                ->select('COUNT(t.id) as count')
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @throws \Doctrine\ORM\Query\QueryException
     */
    private function createActiveQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->addCriteria(
                Criteria::create()
                    ->andWhere(Criteria::expr()->isNull('t.deletedAt'))
                    ->andWhere(
                        Criteria::expr()->orX(
                            Criteria::expr()->eq('t.status', Status::active()),
                            Criteria::expr()->eq('t.status', Status::invisible())
                        )
                    )
            )
            ->innerJoin('t.locations', 'locations')
        ;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(Escort $escort): void
    {
        $this->getEntityManager()->persist($escort);
        $this->getEntityManager()->flush();
    }
}
