<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\ApplicationSupport\ListOfEnumerable;
use App\Escorts\Domain\HairColor as Enum;
use Ds\Map;

final class HairColor extends ListOfEnumerable
{
    public static function getList(): Map
    {
        $map = new Map();

        $map->put(Enum::notSet(), 'Not-set');
        $map->put(Enum::lightBrown(), 'Light Brown');
        $map->put(Enum::blonde(), 'Blonde');
        $map->put(Enum::darkBrown(), 'Dark Brown');
        $map->put(Enum::black(), 'Black');
        $map->put(Enum::auburnRed(), 'Red');
        $map->put(Enum::grayWhite(), 'Gray White');
        $map->put(Enum::wild(), 'Not-set');

        return $map;
    }

    public static function getRawListWithValue(): array
    {
        $enums = [
            Enum::notSet(),
            Enum::lightBrown(),
            Enum::blonde(),
            Enum::darkBrown(),
            Enum::black(),
            Enum::auburnRed(),
            Enum::grayWhite(),
        ];

        $types = [];
        foreach ($enums as $enum) {
            if (Enum::notSet()->equals($enum)) {
                continue;
            }

            $types[] = [
                'value' => $enum->getRawValue(),
                'name' => static::getList()->get($enum),
            ];
        }

        return $types;
    }
}
