<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use Swagger\Annotations as SWG;

final class CityAvailable
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private bool $isAvailable;

    public function __construct(int $id, bool $isAvailable)
    {
        $this->id = $id;
        $this->isAvailable = $isAvailable;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function isAvailable(): bool
    {
        return $this->isAvailable;
    }
}