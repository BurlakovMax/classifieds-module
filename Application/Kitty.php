<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\ApplicationSupport\ListOfEnumerable;
use Ds\Map;
use App\Escorts\Domain\Kitty as Enum;

final class Kitty extends ListOfEnumerable
{
    public static function getList(): Map
    {
        $map = new Map();

        $map->put(Enum::bald(), 'Bald');
        $map->put(Enum::partiallyShaved(), 'Partially Shaved');
        $map->put(Enum::trimmed(), 'Trimmed');
        $map->put(Enum::natural(), 'Natural');
        $map->put(Enum::none(), 'Non-Set');

        return $map;
    }

    public static function getRawListWithValue(): array
    {
        $enums = [
            Enum::bald(),
            Enum::partiallyShaved(),
            Enum::trimmed(),
            Enum::natural(),
            Enum::none(),
        ];

        $types = [];
        foreach ($enums as $enum) {
            if (Enum::none()->equals($enum)) {
                continue;
            }

            $types[] = [
                'value' => $enum->getRawValue(),
                'name' => static::getList()->get($enum),
            ];
        }

        return $types;
    }
}