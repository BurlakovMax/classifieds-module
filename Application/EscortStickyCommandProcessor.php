<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\EmailGate\Domain\EmailData;
use App\Escorts\Domain\AccountReadStorage;
use App\Escorts\Domain\City;
use App\Escorts\Domain\CityReadStorage;
use App\Escorts\Domain\EmailProvider;
use App\Escorts\Domain\Escort;
use App\Escorts\Domain\EscortDoesNotContainsCity;
use App\Escorts\Domain\EscortReadStorage;
use App\Escorts\Domain\EscortService;
use App\Escorts\Domain\EscortSticky;
use App\Escorts\Domain\EscortStickyReadStorage;
use App\Escorts\Domain\EscortStickyWriteStorage;
use App\Escorts\Domain\EscortWaitingListWriteStorage;
use App\Escorts\Domain\Period;
use LazyLemurs\Structures\Email;
use Ramsey\Uuid\Uuid;

final class EscortStickyCommandProcessor
{
    private EscortStickyWriteStorage $escortStickyWriteStorage;

    private EscortStickyReadStorage $escortStickyReadStorage;

    private EscortStickyQueryProcessor $escortStickyQueryProcessor;

    private EscortWaitingListWriteStorage $escortWaitingListWriteStorage;

    private EscortReadStorage $escortReadStorage;

    private EscortService $escortService;

    private CityReadStorage $cityReadStorage;

    private AccountReadStorage $accountReadStorage;

    private EmailProvider $emailProvider;

    private string $fromEmail;

    public function __construct(
        EscortStickyWriteStorage $escortStickyWriteStorage,
        EscortStickyReadStorage $escortStickyReadStorage,
        EscortStickyQueryProcessor $escortStickyQueryProcessor,
        EscortWaitingListWriteStorage $escortWaitingListWriteStorage,
        EscortReadStorage $escortReadStorage,
        EscortService $escortService,
        CityReadStorage $cityReadStorage,
        AccountReadStorage $accountReadStorage,
        EmailProvider $emailProvider,
        string $fromEmail
    ) {
        $this->escortStickyWriteStorage = $escortStickyWriteStorage;
        $this->escortStickyReadStorage = $escortStickyReadStorage;
        $this->escortStickyQueryProcessor = $escortStickyQueryProcessor;
        $this->escortWaitingListWriteStorage = $escortWaitingListWriteStorage;
        $this->escortReadStorage = $escortReadStorage;
        $this->escortService = $escortService;
        $this->cityReadStorage = $cityReadStorage;
        $this->accountReadStorage = $accountReadStorage;
        $this->emailProvider = $emailProvider;
        $this->fromEmail = $fromEmail;
    }

    /**
     * @throws EscortDoesNotContainsCity
     * @throws EscortNotFound
     * @throws \Throwable
     */
    public function activateAfterPaid(int $escortId, int $locationId, int $days): void
    {
        $escort = $this->escortReadStorage->get($escortId);

        if (null === $escort) {
            throw new EscortNotFound();
        }

        if (!$escort->hasLocationsByLocationIds([$locationId])) {
            throw new EscortDoesNotContainsCity();
        }

        $city = $this->cityReadStorage->getById($locationId);
        $escortSticky = $this->escortStickyReadStorage->getByEscortIdAndLocationId($escort->getId(), $locationId);

        if (null === $escortSticky) {
            $this->create($escort, $city, $days);
        } else {
            $this->renew($escortSticky, $escort, $city, $days);
        }
    }

    /**
     * @throws \Throwable
     */
    private function create(Escort $escort, City $city, int $days): void
    {
        $this->escortStickyWriteStorage->add(
            new EscortSticky(
                $escort->getId(),
                $city->getId(),
                $escort->getType(),
                new Period($days)
            )
        );

        $this->updateAfterPaidService($escort, $city, $days);
    }

    /**
     * @throws \Throwable
     */
    private function renew(EscortSticky $escortSticky, Escort $escort, City $city, int $days): void
    {
        $newEscortSticky = $this->escortStickyWriteStorage->getAndLock($escortSticky->getId());
        $newEscortSticky->setDaysInExpireAt($days);

        $this->updateAfterPaidService($escort, $city, $days);
    }

    private function updateAfterPaidService(Escort $escort, City $city, int $days): void
    {
        $this->cancelInEscortWaitingList($escort, $city);

        $escort->activateLocationByLocationId($city->getId());
        $escort->updateAfterPaidService($days, new \DateTimeImmutable('+' . $days . 'days'));

        $this->emailProvider->sendSync(
            new EmailData(
                Uuid::uuid4(),
                new Email($this->fromEmail),
                $this->accountReadStorage->getAccount($escort->getAccountId())->getEmail(),
                'Receipt for your advertisement',
                'Receipt for your advertisement',
                new Email($this->fromEmail)
            )
        );
    }

    private function cancelInEscortWaitingList(Escort $escort, City $city): void
    {
        $escortWaitingList = $this->escortWaitingListWriteStorage->getAndLockByAccountIdAndLocationIdAndType(
            $escort->getAccountId(),
            $city->getId(),
            $escort->getType()
        );

        if (null !== $escortWaitingList) {
            $escortWaitingList->cancel();
        }
    }
}