<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\EscortSide;
use App\Escorts\Domain\EscortType;
use App\Escorts\Domain\Status;
use Swagger\Annotations as SWG;

final class EscortSideData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private int $locationId;

    /**
     * @SWG\Property()
     */
    private int $stateId;

    /**
     * @SWG\Property()
     */
    private int $escortId;

    /**
     * @SWG\Property()
     */
    private int $expireInDays;

    /**
     * @SWG\Property()
     */
    private string $type;

    /**
     * @SWG\Property()
     */
    private int $rawType;

    /**
     * @SWG\Property()
     */
    private \DateTimeImmutable $expireAt;

    /**
     * @SWG\Property()
     */
    private Status $status;

    /**
     * @SWG\Property()
     */
    private string $statusInString;

    public function __construct(EscortSide $escortSide)
    {
        $this->id = $escortSide->getId();
        $this->locationId = $escortSide->getLocationId();
        $this->stateId = $escortSide->getStateId();
        $this->escortId = $escortSide->getEscortId();
        $this->expireInDays = $escortSide->getExpireInDays();
        $this->type = EscortTypeConverter::valueToName($escortSide->getType()->getRawValue());
        $this->rawType = $escortSide->getType()->getRawValue();
        $this->expireAt = $escortSide->getExpireAt();
        $this->status = $escortSide->getStatus();
        $this->statusInString = StatusConverter::valueToName($escortSide->getStatus()->getRawValue());
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getStateId(): int
    {
        return $this->stateId;
    }

    public function getEscortId(): int
    {
        return $this->escortId;
    }

    public function getExpireInDays(): int
    {
        return $this->expireInDays;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getRawType(): int
    {
        return $this->rawType;
    }

    public function getExpireAt(): \DateTimeImmutable
    {
        return $this->expireAt;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function getStatusInString(): string
    {
        return $this->statusInString;
    }
}
