<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\Application\Search\SearchQuery;
use App\Escorts\Domain\EscortSide;
use App\Escorts\Domain\EscortSideAlreadyPurchased;
use App\Escorts\Domain\EscortSideReadStorage;
use App\Escorts\Domain\EscortType;

final class EscortSideQueryProcessor
{
    private EscortSideReadStorage $escortSideReadStorage;

    private int $maximumNumberPerCity;

    private string $price;

    public function __construct(
        EscortSideReadStorage $escortSideReadStorage,
        int $maximumNumberPerCity,
        string $price
    ) {
        $this->escortSideReadStorage = $escortSideReadStorage;
        $this->maximumNumberPerCity = $maximumNumberPerCity;
        $this->price = $price;
    }

    /**
     * @return EscortSideData[]
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $list = $this->escortSideReadStorage->getBySearchQuery($query);

        return $this->mapListToData($list);
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->escortSideReadStorage->countBySearchQuery($query);
    }

    /**
     * @param EscortSide[] $list
     * @return EscortSideData[]
     */
    private function mapListToData(array $list): array
    {
        return array_map(
            function (EscortSide $escortSide): EscortSideData {
                return $this->mapToData($escortSide);
            },
            $list
        );
    }

    private function mapToData(EscortSide $escortSide): EscortSideData
    {
        return new EscortSideData($escortSide);
    }

    /**
     * @param int[] $locationIds
     * @return CityAvailable[]
     *
     * @throws EscortSideAlreadyPurchased
     */
    public function canBuy(int $escortType, array $locationIds, ?int $escortId = null): array
    {
        $availables = [];
        foreach ($locationIds as $locationId) {
            $numberOfSideEscortsPerCity = $this->escortSideReadStorage->countByTypeAndLocationId(EscortType::getValueOf($escortType), $locationId);

            if ($this->maximumNumberPerCity < $numberOfSideEscortsPerCity) {
                $availables[] = new CityAvailable($locationId, false);
                continue;
            }

            $availables[] = new CityAvailable($locationId, true);
        }

        if (null === $escortId) {
            return $availables;
        }

        if ($this->has($escortId)) {
            throw new EscortSideAlreadyPurchased();
        }

        return $availables;
    }

    private function has(int $escortId): bool
    {
        $escortSide = $this->escortSideReadStorage->getByEscortId($escortId);

        return $escortSide !== null;
    }

    public function getPrice(bool $isAgency): string
    {
        if ($isAgency) {
            return (string) ((int) $this->price * 2);
        }

        return $this->price;
    }

    /**
     * @param int[] $locationIds
     */
    public function calculateTotalCoast(bool $isAgency, array $locationIds): string
    {
        return (string) ((int) $this->getPrice($isAgency) * count($locationIds));
    }
}
