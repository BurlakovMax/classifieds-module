<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\EscortType;
use App\Escorts\Domain\EscortWaitingListReadStorage;

final class EscortWaitingListQueryProcessor
{
    private EscortWaitingListReadStorage $escortWaitingListReadStorage;

    public function __construct(EscortWaitingListReadStorage $escortWaitingListReadStorage)
    {
        $this->escortWaitingListReadStorage = $escortWaitingListReadStorage;
    }

    public function hasBy(int $accountId, int $locationId, int $rawType): bool
    {
        $escortWaitingLists = $this->escortWaitingListReadStorage->getBy($accountId, $locationId, EscortType::getValueOf($rawType));

        return !empty($escortWaitingLists);
    }
}