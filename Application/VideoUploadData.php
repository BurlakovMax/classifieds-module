<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use Swagger\Annotations as SWG;

final class VideoUploadData
{
    /**
     * @SWG\Property()
     */
    private string $videoName;

    /**
     * @SWG\Property()
     */
    private string $imageName;

    /**
     * @SWG\Property()
     */
    private string $video;

    /**
     * @SWG\Property()
     */
    private string $image;

    /**
     * @SWG\Property()
     */
    private string $id;

    public function __construct(string $videoName, string $imageName, string $video, string $image, string $id)
    {
        $this->videoName = $videoName;
        $this->imageName = $imageName;
        $this->video = $video;
        $this->image = $image;
        $this->id = $id;
    }

    public function getVideoName(): string
    {
        return $this->videoName;
    }

    public function getImageName(): string
    {
        return $this->imageName;
    }

    public function getVideo(): string
    {
        return $this->video;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getId(): string
    {
        return $this->id;
    }
}