<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\BodyType;
use App\Escorts\Domain\EmailVisibility;
use App\Escorts\Domain\Ethnicity;
use App\Escorts\Domain\EyeColor;
use App\Escorts\Domain\HairColor;
use App\Escorts\Domain\Kitty;
use LazyLemurs\Commander\Property;

abstract class EscortCommand
{
    private ?int $accountId;

    /**
     * @var int[]
     *
     * @Property(type="int[]")
     */
    private array $locationIds;

    /**
     * @var int[]
     */
    private array $stateIds = [];

    /**
     * @Property()
     */
    private string $firstName;

    /**
     * @Property()
     */
    private ?string $middleName;

    /**
     * @Property()
     */
    private ?string $lastName;

    /**
     * @Property()
     */
    private string $title;

    /**
     * @Property()
     */
    private string $content;

    /**
     * @Property()
     */
    private ?bool $isAvailableForMen;

    /**
     * @Property()
     */
    private ?bool $isAvailableForWomen;

    /**
     * @Property()
     */
    private ?bool $isAvailableForCouple;

    /**
     * @Property()
     */
    private ?bool $isAvailableForGroup;

    /**
     * @Property()
     */
    private ?bool $isAvailableForBlack;

    /**
     * @Property()
     */
    private ?bool $isIncallEnabled;

    /**
     * @Property()
     */
    private ?int $incallRateHalfHour;

    /**
     * @Property()
     */
    private ?int $incallRateOneHour;

    /**
     * @Property()
     */
    private ?int $incallRateTwoHours;

    /**
     * @Property()
     */
    private ?int $incallRateOvernight;

    /**
     * @Property()
     */
    private ?bool $isOutcallEnabled;

    /**
     * @Property()
     */
    private ?int $outcallRateHalfHour;

    /**
     * @Property()
     */
    private ?int $outcallRateOneHour;

    /**
     * @Property()
     */
    private ?int $outcallRateTwoHours;

    /**
     * @Property()
     */
    private ?int $outcallRateOvernight;

    /**
     * @Property()
     */
    private ?bool $isVisiting;

    /**
     * @Property()
     */
    private ?\DateTimeImmutable $visitingFrom;

    /**
     * @Property()
     */
    private ?\DateTimeImmutable $visitingTo;

    /**
     * @Property()
     */
    private ?string $placeDescription;

    /**
     * @Property()
     */
    private ?bool $gfe;

    /**
     * @Property()
     */
    private ?bool $gfeLimited;

    /**
     * @Property()
     */
    private ?bool $tantra;

    /**
     * @Property()
     */
    private ?bool $fetishDominant;

    /**
     * @Property()
     */
    private ?bool $fetishSubmissive;

    /**
     * @Property()
     */
    private ?bool $fetishSwitch;

    /**
     * @Property()
     */
    private ?string $fetish;

    /**
     * @Property()
     */
    private ?string $phone;

    /**
     * @Property()
     */
    private ?string $email;

    private ?EmailVisibility $emailVisibility;

    /**
     * @Property()
     */
    private ?string $website;

    /**
     * @Property()
     */
    private ?string $facebook;

    /**
     * @Property()
     */
    private ?string $twitter;

    /**
     * @Property()
     */
    private ?string $telegram;

    /**
     * @Property()
     */
    private ?string $google;

    /**
     * @Property()
     */
    private ?string $instagram;

    /**
     * @Property()
     */
    private ?string $wechat;

    /**
     * @Property()
     */
    private ?string $whatsapp;

    /**
     * @Property()
     */
    private int $age;

    private ?Ethnicity $ethnicity;

    /**
     * @Property(type="int[]")
     */
    private array $languageIds;

    /**
     * @Property()
     */
    private ?int $heightFeet;

    /**
     * @Property()
     */
    private ?int $heightInches;

    /**
     * @Property()
     */
    private ?int $weight;

    private ?HairColor $hairColor;

    private ?EyeColor $eyeColor;

    private ?BodyType $bodyType;

    /**
     * @Property()
     */
    private ?int $bust;

    /**
     * @Property()
     */
    private ?int $waist;

    /**
     * @Property()
     */
    private ?int $hip;

    /**
     * @Property()
     */
    private ?int $cupSize;

    /**
     * @Property()
     */
    private ?int $penisSize;

    private ?Kitty $kitty;

    /**
     * @Property()
     */
    private ?bool $isPornstar;

    /**
     * @Property()
     */
    private ?bool $isPregnant;

    /**
     * @Property()
     */
    private ?string $terId;

    /**
     * @Property()
     */
    private ?bool $isVisaAccepted;

    /**
     * @Property()
     */
    private ?bool $isAmexAccepted;

    /**
     * @Property()
     */
    private ?bool $isDiscoverAccepted;

    /**
     * @var string[]
     *
     * @Property(type="string[]")
     */
    private array $images;

    /**
     * @var string[]
     *
     * @Property(type="string[]")
     */
    private array $videos;

    public function __construct(
        ?int $accountId
    ) {
        $this->accountId = $accountId;
        $this->emailVisibility = null;
    }

    public function setEmailVisibility(/** mixed */ $emailVisibility): void
    {
        if ($emailVisibility === '' || null === $emailVisibility) {
            $this->emailVisibility = null;
            return;
        }

        $this->emailVisibility = EmailVisibility::getValueOf((int)$emailVisibility);
    }

    public function setEthnicity(/** mixed */ $ethnicity): void
    {
        if ($ethnicity === '') {
            $this->ethnicity = null;
            return;
        }

        $this->ethnicity = Ethnicity::getValueOf((int)$ethnicity);
    }

    public function setHairColor(/** mixed */ $hairColor): void
    {
        if ($hairColor === '') {
            $this->hairColor = null;
            return;
        }

        $this->hairColor = HairColor::getValueOf((int)$hairColor);
    }

    public function setEyeColor(/** mixed */ $eyeColor): void
    {
        if ($eyeColor === '') {
            $this->eyeColor = null;
            return;
        }

        $this->eyeColor = EyeColor::getValueOf((int)$eyeColor);
    }

    public function setBodyType(/** mixed */ $bodyType): void
    {
        if ($bodyType === '') {
            $this->bodyType = null;
            return;
        }

        $this->bodyType = BodyType::getValueOf((int)$bodyType);
    }

    public function setKitty(/** mixed */ $kitty): void
    {
        if ($kitty === '') {
            $this->kitty = null;
            return;
        }

        $this->kitty = Kitty::getValueOf((int)$kitty);
    }

    /**
     * @return int[]
     */
    public function getStateIds(): array
    {
        return $this->stateIds;
    }

    /**
     * @param int[] $stateIds
     */
    public function setStateIds(array $stateIds): void
    {
        $this->stateIds = $stateIds;
    }

    public function getAccountId(): ?int
    {
        return $this->accountId;
    }

    /**
     * @return int[]
     */
    public function getLocationIds(): array
    {
        return $this->locationIds;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getIsAvailableForMen(): bool
    {
        if (null === $this->isAvailableForMen) {
            return false;
        }

        return $this->isAvailableForMen;
    }

    public function getIsAvailableForWomen(): bool
    {
        if (null === $this->isAvailableForWomen) {
            return false;
        }

        return $this->isAvailableForWomen;
    }

    public function getIsAvailableForCouple(): bool
    {
        if (null === $this->isAvailableForCouple) {
            return false;
        }

        return $this->isAvailableForCouple;
    }

    public function getIsAvailableForGroup(): bool
    {
        if (null === $this->isAvailableForGroup) {
            return false;
        }

        return $this->isAvailableForGroup;
    }

    public function getIsAvailableForBlack(): bool
    {
        if (null === $this->isAvailableForBlack) {
            return false;
        }

        return $this->isAvailableForBlack;
    }

    public function getIsIncallEnabled(): bool
    {
        if (null === $this->isIncallEnabled) {
            return false;
        }

        return $this->isIncallEnabled;
    }

    public function getIncallRateHalfHour(): ?int
    {
        return $this->incallRateHalfHour;
    }

    public function getIncallRateOneHour(): ?int
    {
        return $this->incallRateOneHour;
    }

    public function getIncallRateTwoHours(): ?int
    {
        return $this->incallRateTwoHours;
    }

    public function getIncallRateOvernight(): ?int
    {
        return $this->incallRateOvernight;
    }

    public function getIsOutcallEnabled(): bool
    {
        if (null === $this->isOutcallEnabled) {
            return false;
        }

        return $this->isOutcallEnabled;
    }

    public function getOutcallRateHalfHour(): ?int
    {
        return $this->outcallRateHalfHour;
    }

    public function getOutcallRateOneHour(): ?int
    {
        return $this->outcallRateOneHour;
    }

    public function getOutcallRateTwoHours(): ?int
    {
        return $this->outcallRateTwoHours;
    }

    public function getOutcallRateOvernight(): ?int
    {
        return $this->outcallRateOvernight;
    }

    public function getIsVisiting(): bool
    {
        if (null === $this->isVisiting) {
            return false;
        }

        return $this->isVisiting;
    }

    public function getVisitingFrom(): ?\DateTimeImmutable
    {
        return $this->visitingFrom;
    }

    public function getVisitingTo(): ?\DateTimeImmutable
    {
        return $this->visitingTo;
    }

    public function getPlaceDescription(): ?string
    {
        return $this->placeDescription;
    }

    public function getGfe(): bool
    {
        if (null === $this->gfe) {
            return false;
        }

        return $this->gfe;
    }

    public function getGfeLimited(): bool
    {
        if (null === $this->gfeLimited) {
            return false;
        }

        return $this->gfeLimited;
    }

    public function getTantra(): bool
    {
        if (null === $this->tantra) {
            return false;
        }

        return $this->tantra;
    }

    public function getFetishDominant(): bool
    {
        if (null === $this->fetishDominant) {
            return false;
        }

        return $this->fetishDominant;
    }

    public function getFetishSubmissive(): bool
    {
        if (null === $this->fetishSubmissive) {
            return false;
        }

        return $this->fetishSubmissive;
    }

    public function getFetishSwitch(): bool
    {
        if (null === $this->fetishSwitch) {
            return false;
        }

        return $this->fetishSwitch;
    }

    public function getFetish(): ?string
    {
        return $this->fetish;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getEmailVisibility(): EmailVisibility
    {
        if (null === $this->emailVisibility) {
            return EmailVisibility::anonymous();
        }

        return $this->emailVisibility;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function getTelegram(): ?string
    {
        return $this->telegram;
    }

    public function getGoogle(): ?string
    {
        return $this->google;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function getWechat(): ?string
    {
        return $this->wechat;
    }

    public function getWhatsapp(): ?string
    {
        return $this->whatsapp;
    }

    public function getAge(): int
    {
        return $this->age;
    }

    public function getEthnicity(): Ethnicity
    {
        return $this->ethnicity ?? Ethnicity::notSet();
    }

    /**
     * @return int[]
     */
    public function getLanguageIds(): array
    {
        return $this->languageIds;
    }

    public function getHeightFeet(): ?int
    {
        return $this->heightFeet ?? 0;
    }

    public function getHeightInches(): int
    {
        return $this->heightInches ?? 0;
    }

    public function getWeight(): int
    {
        return $this->weight ?? 0;
    }

    public function getHairColor(): HairColor
    {
        return $this->hairColor ?? HairColor::notSet();
    }

    public function getEyeColor(): EyeColor
    {
        return $this->eyeColor ?? EyeColor::notSet();
    }

    public function getBodyType(): BodyType
    {
        return $this->bodyType ?? BodyType::none();
    }

    public function getBust(): int
    {
        return $this->bust ?? 0;
    }

    public function getWaist(): int
    {
        return $this->waist ?? 0;
    }

    public function getHip(): int
    {
        return $this->hip ?? 0;
    }

    public function getCupSize(): int
    {
        return $this->cupSize ?? 0;
    }

    public function getPenisSize(): ?int
    {
        return $this->penisSize;
    }

    public function getKitty(): Kitty
    {
        return $this->kitty ?? Kitty::none();
    }

    public function getIsPornstar(): bool
    {
        if (null === $this->isPornstar) {
            return false;
        }

        return $this->isPornstar;
    }

    public function getIsPregnant(): bool
    {
        if (null === $this->isPregnant) {
            return false;
        }

        return $this->isPregnant;
    }

    public function getTerId(): ?string
    {
        return $this->terId;
    }

    public function getIsVisaAccepted(): bool
    {
        if (null === $this->isVisaAccepted) {
            return false;
        }

        return $this->isVisaAccepted;
    }

    public function getIsAmexAccepted(): bool
    {
        if (null === $this->isAmexAccepted) {
            return false;
        }

        return $this->isAmexAccepted;
    }

    public function getIsDiscoverAccepted(): bool
    {
        if (null === $this->isDiscoverAccepted) {
            return false;
        }

        return $this->isDiscoverAccepted;
    }

    /**
     * @param string[] $images
     */
    public function attachImages(array $images): void
    {
        $this->images = $images;
    }

    /**
     * @return string[]
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @return string[]
     */
    public function getVideos(): array
    {
        return $this->videos;
    }
}
