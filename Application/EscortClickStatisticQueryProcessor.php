<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\EscortClickStatistic;
use App\Escorts\Domain\EscortClickStorage;

final class EscortClickStatisticQueryProcessor
{
    private EscortClickStorage $storage;

    public function __construct(EscortClickStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return EscortClickStatisticData[]
     */
    public function getByEscortId(int $escortId, \DateTimeImmutable $dateFrom, \DateTimeImmutable $dateTo): array
    {
        return $this->mapListToData($this->storage->getByEscortId($escortId, $dateFrom, $dateTo));
    }

    /**
     * @param EscortClickStatistic[]
     * @return EscortClickStatisticData[]
     */
    private function mapListToData(array $escortStatistic): array
    {
        return array_map(
            fn($statistic): EscortClickStatisticData => new EscortClickStatisticData($statistic), $escortStatistic
        );
    }
}
