<?php

declare(strict_types=1);

namespace App\Escorts\Application;

final class LocationData
{
    private int $cityId;

    private string $cityName;

    private string $cityUrl;

    private string $stateName;

    private string $countryName;

    public function __construct(
        int $cityId,
        string $cityName,
        string $cityUrl,
        string $stateName,
        string $countryName
    ) {
        $this->cityId = $cityId;
        $this->cityName = $cityName;
        $this->cityUrl = $cityUrl;
        $this->stateName = $stateName;
        $this->countryName = $countryName;
    }

    public function getCityId(): int
    {
        return $this->cityId;
    }

    public function getCityName(): string
    {
        return $this->cityName;
    }

    public function getCityUrl(): string
    {
        return $this->cityUrl;
    }

    public function getStateName(): string
    {
        return $this->stateName;
    }

    public function getCountryName(): string
    {
        return $this->countryName;
    }
}