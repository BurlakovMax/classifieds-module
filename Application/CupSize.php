<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\ApplicationSupport\ListOfEnumerable;
use Ds\Map;
use App\Escorts\Domain\CupSize as Enum;

final class CupSize extends ListOfEnumerable
{
    public static function getList(): Map
    {
        $map = new Map();

        $map->put(Enum::A(), 'A');
        $map->put(Enum::B(), 'B');
        $map->put(Enum::C(), 'C');
        $map->put(Enum::D(), 'D');
        $map->put(Enum::DD(), 'DD');
        $map->put(Enum::DDD(), 'DDD');
        $map->put(Enum::plainHuge(), 'Just Plain Huge');
        $map->put(Enum::none(), 'Not set');

        return $map;
    }

    public static function getRawListWithValue(): array
    {
        return array_map(
            function (Enum $type): array {
                return [
                    'value' => $type->getRawValue(),
                    'name' => static::getList()->get($type),
                ];
            },
            [
                Enum::A(),
                Enum::B(),
                Enum::C(),
                Enum::D(),
                Enum::DD(),
                Enum::DDD(),
                Enum::plainHuge(),
                Enum::none(),
            ]
        );
    }
}
