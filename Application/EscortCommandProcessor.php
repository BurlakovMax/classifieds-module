<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\CreateEscortResult;
use App\Escorts\Domain\EscortService;
use LazyLemurs\Structures\Email;
use LazyLemurs\TransactionManager\TransactionManager;

final class EscortCommandProcessor
{
    private EscortService $escortService;

    private TransactionManager $transactionManager;

    public function __construct(
        EscortService $escortService,
        TransactionManager $transactionManager
    ) {
        $this->escortService = $escortService;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @throws \Throwable
     */
    public function create(CreateEscortCommand $command): CreateEscortResultData
    {
        return $this->transactionManager->transactional(function () use ($command): CreateEscortResultData {
            $result = $this->escortService->add($command);

            return new CreateEscortResultData(
                $result->getId(),
                $result->getLinkToken()
            );
        });
    }

    /**
     * @throws \Throwable
     */
    public function update(UpdateEscortCommand $command): void
    {
        $this->transactionManager->transactional(function () use ($command): void {
            $this->escortService->update($command);
        });
    }

    public function adminUpdate(UpdateEscortAdminCommand $command): void
    {
        $this->transactionManager->transactional(function () use ($command): void {
            $this->escortService->adminUpdate($command);
        });
    }

    /**
     * @throws \Throwable
     */
    public function sendEmail(int $escortId, Email $email, string $message): void
    {
        $this->transactionManager->transactional(function () use ($escortId, $email, $message): void {
            $this->escortService->sendEmail($escortId, $email, $message);
        });
    }

    /**
     * @throws \Throwable
     */
    public function linkToAccount(int $accountId, int $escortId): void
    {
        $this->transactionManager->transactional(function () use ($accountId, $escortId): void {
            $this->escortService->linkToAccount($accountId, $escortId);
        });
    }

    /**
     * @throws EscortNotFound
     * @throws \Throwable
     */
    public function moveToTop(int $escortId): void
    {
        $this->transactionManager->transactional(function () use ($escortId): void {
            $this->escortService->moveEscortToTop($escortId);
        });
    }

    /**
     * @throws EscortNotFound
     * @throws \Throwable
     */
    public function removeEscort(int $escortId): void
    {
        $this->transactionManager->transactional(function () use ($escortId): void {
            $this->escortService->removeEscort($escortId);
        });
    }

    public function changeLocation(int $escortId, int $fromLocationId, int $toLocationId): void
    {
        $this->transactionManager->transactional(function () use (
            $escortId,
            $fromLocationId,
            $toLocationId
        ): void {
            $this->escortService->changeLocation($escortId, $fromLocationId, $toLocationId);
        });
    }

    /**
     * @param string[] $images
     * @throws \Throwable
     */
    public function removeImages(int $escortId, array $images): void
    {
        $this->transactionManager->transactional(function () use ($escortId, $images): void {
            $this->escortService->removeImages($escortId, $images);
        });
    }

    /**
     * @param string[] $videos
     * @throws \Throwable
     */
    public function removeVideos(int $escortId, array $videos): void
    {
        $this->transactionManager->transactional(function () use ($escortId, $videos): void {
            $this->escortService->removeVideos($escortId, $videos);
        });
    }

    public function setThumbnail(int $escortId, string $thumbnailName): void
    {
        $this->transactionManager->transactional(function () use ($escortId, $thumbnailName): void {
           $this->escortService->setThumbnail($escortId, $thumbnailName);
        });
    }

    /**
     * @throws EscortNotFound
     */
    public function duplicate(int $escortId, int $escortType): CreateEscortResult
    {
        return $this->transactionManager->transactional(function () use ($escortId, $escortType): CreateEscortResult {
            return $this->escortService->duplicate($escortId, $escortType);
        });
    }
}
