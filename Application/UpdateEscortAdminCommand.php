<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use LazyLemurs\Commander\Property;
use App\Escorts\Domain\EscortType;
use App\Escorts\Domain\Status;

final class UpdateEscortAdminCommand
{
    private int $id;

    /**
     * @Property()
     */
    private int $type;

    /**
     * @Property()
     */
    private int $status;

    /**
     * @Property()
     */
    private int $accountId;

    /**
     * @Property()
     */
    private ?bool $sponsor;

    /**
     * @Property()
     */
    private ?bool $sponsorMobile;

    /**
     * @Property()
     */
    private int $sponsorPosition;

    /**
     * @Property()
     */
    private \DateTimeImmutable $expireStamp;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): EscortType
    {
        return EscortType::getValueOf($this->type);
    }

    public function getStatus(): Status
    {
        return Status::getValueOf($this->status);
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function isSponsor(): bool
    {
        return $this->sponsor;
    }

    public function isSponsorMobile(): bool
    {
        return null !== $this->sponsorMobile;
    }

    public function getSponsorPosition(): int
    {
        return $this->sponsorPosition;
    }

    public function getExpireStamp(): \DateTimeImmutable
    {
        return $this->expireStamp;
    }
}
