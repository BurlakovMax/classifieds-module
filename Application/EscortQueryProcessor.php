<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Escorts\Domain\CitiesIsEmpty;
use App\Escorts\Domain\City;
use App\Escorts\Domain\CityReadStorage;
use App\Escorts\Domain\Escort;
use App\Escorts\Domain\EscortDoesNotContainsCity;
use App\Escorts\Domain\EscortLocation;
use App\Escorts\Domain\EscortReadStorage;
use App\Escorts\Domain\EscortSideReadStorage;
use App\Escorts\Domain\EscortSponsorReadStorage;
use App\Escorts\Domain\EscortType;
use LazyLemurs\Exceptions\NotFoundException;

final class EscortQueryProcessor
{
    private const TOP_UPS_OPTIONS = [
        1, 2, 3, 5, 10, 20, 30, 50, 100
    ];

    private EscortReadStorage $escortReadStorage;

    private EscortSideReadStorage $escortSideReadStorage;

    private EscortSponsorReadStorage $escortSponsorReadStorage;

    private CityReadStorage $cityReadStorage;

    private string $escortPrice;

    private string $escortTopUpPrice;

    private string $escortTopUpForAgencyPrice;

    public function __construct(
        EscortReadStorage $escortReadStorage,
        EscortSideReadStorage $escortSideReadStorage,
        EscortSponsorReadStorage $escortSponsorReadStorage,
        CityReadStorage $cityReadStorage,
        string $escortPrice,
        string $escortTopUpPrice,
        string $escortTopUpForAgencyPrice
    ) {
        $this->escortReadStorage = $escortReadStorage;
        $this->escortSideReadStorage = $escortSideReadStorage;
        $this->escortSponsorReadStorage = $escortSponsorReadStorage;
        $this->cityReadStorage = $cityReadStorage;
        $this->escortPrice = $escortPrice;
        $this->escortTopUpPrice = $escortTopUpPrice;
        $this->escortTopUpForAgencyPrice = $escortTopUpForAgencyPrice;
    }

    public function getEscortPrice(): string
    {
        return $this->escortPrice;
    }

    /**
     * @return LocationData[]
     * @throws NotFoundException
     */
    public function getLocations(int $escortId, int $accountId): array
    {
        return array_map([$this, 'mapToLocationData'], $this->getEscortLocation($escortId, $accountId));
    }

    /**
     * @param int ...$ids
     * @return City[]
     */
    public function getLocationsByIds(int ...$ids): array
    {
        return $this->cityReadStorage->getByIds(...$ids);
    }

    private function mapToLocationData(City $city): LocationData
    {
        return
            new LocationData(
                $city->getId(),
                $city->getName(),
                $city->getUrl(),
                $city->getStateName(),
                $city->getCountryName(),
            )
        ;
    }

    /**
     * @return City[]
     * @throws NotFoundException
     */
    public function getEscortLocation(int $escortId, int $accountId): array
    {
        $escort = $this->getActiveByIdAndAccountId($escortId, $accountId);

        $locations = [];
        foreach ($escort->getLocations() as $location) {
            $locations[$location->getLocationId()] = $this->cityReadStorage->getById($location->getLocationId());
        }

        return $locations;
    }

    public function getEscortTopUpPrice(bool $isAgency): string
    {
        if ($isAgency) {
            return $this->escortTopUpForAgencyPrice;
        }

        return $this->escortTopUpPrice;
    }

    /**
     * @return array|int[]
     */
    public function getTopUpsOptions(): array
    {
        return self::TOP_UPS_OPTIONS;
    }

    /**
     * @throws EscortNotFound
     */
    public function getActiveByIdAndAccountId(int $id, int $accountId): EscortData
    {
        $escort = $this->escortReadStorage->getActiveByIdAndAccountId($id, $accountId);

        if (null === $escort) {
            throw new EscortNotFound();
        }

        return $this->mapToData($escort, ...$this->mapToEscortLocationDataList(...$escort->getLocations()));
    }

    public function getLastActiveByAccountId(int $accountId): EscortData
    {
        $escort = $this->escortReadStorage->getLastActiveByAccountId($accountId);

        if (null === $escort) {
            throw new EscortNotFound();
        }

        return $this->mapToData($escort);
    }

    /**
     * @param int[] $locationIds
     *
     * @throws CitiesIsEmpty
     * @throws EscortDoesNotContainsCity
     * @throws EscortNotFound
     */
    public function getActiveByIdAndAccountIdForBuy(int $id, int $accountId, array $locationIds): EscortData
    {
        if (empty($locationIds)) {
            throw new CitiesIsEmpty();
        }

        $escort = $this->escortReadStorage->getActiveByIdAndAccountId($id, $accountId);

        if (null === $escort) {
            throw new EscortNotFound();
        }

        if (!$escort->hasLocationsByLocationIds($locationIds)) {
            throw new EscortDoesNotContainsCity();
        }

        return $this->mapToData($escort, ...$this->mapToEscortLocationDataList(...$escort->getLocations()));
    }

    /**
     * @throws EscortNotFound
     */
    public function getById(int $id): EscortData
    {
        $escort = $this->get($id);

        return $this->mapToData($escort, ...$this->mapToEscortLocationDataList(...$escort->getLocations()));
    }

    /**
     * @return int[]
     */
    public function getIdsBySearchQuery(SearchQuery $query): array
    {
        return $this->escortReadStorage->getIdsBySearchQuery($query);
    }

    public function countIdsBySearchQuery(SearchQuery $query): int
    {
        return $this->escortReadStorage->countIdsBySearchQuery($query);
    }

    /**
     * @return EscortData[]
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $list = $this->escortReadStorage->getBySearchQuery($query);

        return $this->mapListToData($list);
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->escortReadStorage->countBySearchQuery($query);
    }

    /**
     * @return EscortData[]
     */
    public function getEscortSponsorsByLocationId(int $locationId, int $limit): array
    {
        $escortIds = $this->escortSponsorReadStorage->getEscortIdsByLocationId($locationId);
        shuffle($escortIds);
        $list = $this->escortReadStorage->getByIds(array_slice($escortIds, 0, $limit));

        return $this->mapListToData($list);
    }

    /**
     * @param int[] $ids
     * @return EscortData[]
     */
    public function getByIds(array $ids): array
    {
        $list = $this->escortReadStorage->getByIds($ids);

        return $this->mapListToData($list);
    }

    /**
     * @param FilterQuery[] $filters
     */
    public function countByFilterQuery(array $filters): int
    {
        return $this->escortReadStorage->countByFilterQuery($filters);
    }

    /**
     * @return EscortData[]
     */
    public function getEscortsSideByTypeAndLocationId(EscortType $type, int $locationId): array
    {
        $escortIds = $this->escortSideReadStorage->getEscortIdsByTypeAndLocationId($type, $locationId);
        $list = $this->escortReadStorage->getByIds($escortIds);

        return $this->mapListToData($list);
    }

    /**
     * @return EscortData[]
     */
    public function getEscortAdvertisingByTypeAndLocationId(EscortType $type, int $locationId): array
    {
        return
            $this->getBySearchQuery(
                new SearchQuery(
                    new FuzzySearchQuery(null, []),
                    array_merge(
                        [
                            FilterQuery::createEqFilter('type', $type),
                            FilterQuery::createEqFilter('locations.locationId', $locationId),
                            FilterQuery::createEqFilter('t.sponsorAttributes.sponsor', false),
                            FilterQuery::createEqFilter('t.sponsorAttributes.sponsorMobile', true),
                        ],
                    ),
                    [new OrderQuery('t.sponsorAttributes.sponsorPosition', Ordering::asc())],
                    LimitationQueryImmutable::create(null, null)
                )
            );
    }

    /**
     * @throws EscortNotFound
     */
    private function get(int $id): Escort
    {
        $escort = $this->escortReadStorage->get($id);

        if (!$escort) {
            throw new EscortNotFound();
        }

        return $escort;
    }

    /**
     * @param Escort[] $list
     * @return EscortData[]
     */
    private function mapListToData(array $list): array
    {
        return array_map(
            function (Escort $escort): EscortData {
                return $this->mapToData($escort, ...$this->mapToEscortLocationDataList(...$escort->getLocations()));
            },
            $list
        );
    }


    /**
     * @param EscortLocation ...$locations
     * @return EscortData[]
     */
    private function mapToEscortLocationDataList(EscortLocation ...$locations): array
    {
        return
            array_map(
                function (EscortLocation $location): EscortLocationData {
                    return $this->mapToEscortLocationData(
                        $location,
                        $this->cityReadStorage->getById($location->getLocationId())
                    );
                },
                $locations
            )
        ;
    }

    private function mapToEscortLocationData(EscortLocation $location, City $city): EscortLocationData
    {
        return new EscortLocationData($location, $city);
    }

    private function mapToData(Escort $escort, EscortLocationData ...$escortLocations): EscortData
    {
        return new EscortData($escort, ...$escortLocations);
    }
}
