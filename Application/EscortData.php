<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\Escort;
use App\Escorts\Domain\EscortImage;
use App\Escorts\Domain\EscortVideo;
use App\Escorts\Domain\Language;
use DateTimeImmutable;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

final class EscortData
{
    /**
     * @SWG\Property()
     */
    private ?string $bodyType;

    /**
     * @SWG\Property()
     */
    private ?int $bodyTypeRawValue;

    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private ?int $stateId;

    /**
     * @SWG\Property()
     */
    private string $type;

    /**
     * @SWG\Property()
     */
    private int $rawType;

    /**
     * @SWG\Property()
     */
    private bool $isShemale;

    /**
     * @var EscortLocationData[]
     *
     * @SWG\Property(
     *     property="locations",
     *     type="array",
     *     @SWG\Items(ref=@Model(type=App\Escorts\Application\EscortLocationData::class))
     * )
     */
    private array $locations;

    /**
     * @var int[]
     *
     * @SWG\Property(property="locationIds", type="array", @SWG\Items(type="int"))
     */
    private array $locationIds;

    /**
     * @SWG\Property()
     */
    private ?int $accountId;

    /**
     * @SWG\Property()
     */
    private ?int $status;

    /**
     * @SWG\Property()
     */
    private ?string $statusInString;

    /**
     * @SWG\Property()
     */
    private ?\DateTimeImmutable $expireStamp;

    /**
     * @SWG\Property()
     */
    private ?string $ethnicity;

    /**
     * @SWG\Property()
     */
    private ?int $ethnicityRawValue;

    /**
     * @SWG\Property()
     */
    private ?string $phone;

    /**
     * @SWG\Property()
     */
    private ?string $email;

    /**
     * @SWG\Property()
     */
    private int $emailVisibility;

    /**
     * @SWG\Property()
     */
    private ?string $website;

    /**
     * @SWG\Property()
     */
    private string $title;

    /**
     * @SWG\Property()
     */
    private ?string $placeDescription;

    /**
     * @SWG\Property()
     */
    private DateTimeImmutable $postedAt;

    /**
     * @SWG\Property()
     */
    private string $content;

    /**
     * @SWG\Property()
     */
    private ?string $thumbnail;

    /**
     * @SWG\Property()
     */
    private ?string $multipleThumbnails;

    /**
     * @SWG\Property()
     */
    private bool $isIncallEnabled;

    /**
     * @SWG\Property()
     */
    private int $incallRateHalfHour;

    /**
     * @SWG\Property()
     */
    private int $incallRateOneHour;

    /**
     * @SWG\Property()
     */
    private int $incallRateTwoHours;

    /**
     * @SWG\Property()
     */
    private int $incallRateOvernight;

    /**
     * @SWG\Property()
     */
    private ?int $incallRate;

    /**
     * @SWG\Property()
     */
    private bool $isOutcallEnabled;

    /**
     * @SWG\Property()
     */
    private int $outcallRateHalfHour;

    /**
     * @SWG\Property()
     */
    private int $outcallRateOneHour;

    /**
     * @SWG\Property()
     */
    private int $outcallRateTwoHours;

    /**
     * @SWG\Property()
     */
    private int $outcallRateOvernight;

    /**
     * @SWG\Property()
     */
    private ?int $outcallRate;

    /**
     * @SWG\Property()
     */
    private ?int $available;

    /**
     * @SWG\Property()
     */
    private ?string $facebook;

    /**
     * @SWG\Property()
     */
    private ?string $twitter;

    /**
     * @SWG\Property()
     */
    private ?string $telegram;

    /**
     * @SWG\Property()
     */
    private ?string $google;

    /**
     * @SWG\Property()
     */
    private ?string $instagram;

    /**
     * @SWG\Property()
     */
    private ?string $wechat;

    /**
     * @SWG\Property()
     */
    private ?string $whatsapp;

    /**
     * @SWG\Property()
     */
    private ?int $age;

    /**
     * @SWG\Property()
     */
    private bool $isPornstar;

    /**
     * @SWG\Property()
     */
    private bool $isVisaAccepted;

    /**
     * @SWG\Property()
     */
    private bool $isAmexAccepted;

    /**
     * @SWG\Property()
     */
    private bool $isDiscoverAccepted;

    /**
     * @SWG\Property()
     */
    private bool $isVisiting;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $visitingFrom;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $visitingTo;

    /**
     * @SWG\Property()
     */
    private ?string $hairColor;

    private ?int $hairColorRawValue;

    /**
     * @SWG\Property()
     */
    private ?string $eyeColor;

    private ?int $eyeColorRawValue;

    /**
     * @SWG\Property()
     */
    private bool $isPregnant;

    /**
     * @SWG\Property()
     */
    private bool $tantra;

    /**
     * @SWG\Property()
     */
    private ?string $firstName;

    /**
     * @SWG\Property()
     */
    private ?string $middleName;

    /**
     * @SWG\Property()
     */
    private ?string $lastName;

    /**
     * @SWG\Property()
     */
    private ?bool $autoRenew;

    /**
     * @SWG\Property()
     */
    private ?int $autoRenewFrequency;

    /**
     * @SWG\Property()
     */
    private int $repostLeft;

    /**
     * @SWG\Property()
     */
    private ?int $autoRenewTime;

    /**
     * @SWG\Property()
     */
    private bool $sponsor;

    /**
     * @SWG\Property()
     */
    private bool $sponsorMobile;

    /**
     * @SWG\Property()
     */
    private ?int $sponsorPosition;

    /**
     * @SWG\Property()
     */
    private ?int $sponsorLocId;

    /**
     * @SWG\Property()
     */
    private bool $allowLinks;

    /**
     * @SWG\Property()
     */
    private bool $isAvailableForMen;

    /**
     * @SWG\Property()
     */
    private bool $isAvailableForWomen;

    /**
     * @SWG\Property()
     */
    private bool $isAvailableForCouple;

    /**
     * @SWG\Property()
     */
    private bool $isAvailableForGroup;

    /**
     * @SWG\Property()
     */
    private bool $isAvailableForBlack;

    /**
     * @SWG\Property()
     */
    private bool $fetishDominant;

    /**
     * @SWG\Property()
     */
    private bool $fetishSubmissive;

    /**
     * @SWG\Property()
     */
    private bool $fetishSwitch;

    /**
     * @SWG\Property()
     */
    private int $heightFeet;

    /**
     * @SWG\Property()
     */
    private int $heightInches;

    /**
     * @SWG\Property()
     */
    private int $weight;

    /**
     * @SWG\Property()
     */
    private int $bust;

    /**
     * @SWG\Property()
     */
    private int $waist;

    /**
     * @SWG\Property()
     */
    private int $hip;

    /**
     * @SWG\Property()
     */
    private ?string $cupSize;

    /**
     * @SWG\Property()
     */
    private int $cupSizeRawValue;

    /**
     * @SWG\Property()
     */
    private ?int $penisSize;

    /**
     * @SWG\Property()
     */
    private ?string $kitty;

    /**
     * @SWG\Property()
     */
    private ?int $kittyRawValue;

    /**
     * @SWG\Property()
     */
    private ?string $fetish;

    /**
     * @SWG\Property()
     */
    private bool $gfe;

    /**
     * @SWG\Property()
     */
    private bool $gfeLimited;

    /**
     * @var int[]
     *
     * @SWG\Property(property="languageIds", type="array", @SWG\Items(type="int"))
     */
    private array $languageIds;

    /**
     * @var string[]
     *
     * @SWG\Property(
     *     property="images",
     *     type="array",
     *     @SWG\Items(type="string")
     * )
     */
    private array $images;

    /**
     * @var string[]
     *
     * @SWG\Property(
     *     property="videos",
     *     type="array",
     *     @SWG\Items(type="string"))
     * )
     */
    private array $videos;

    /**
     * @SWG\Property()
     */
    private bool $verified;

    /**
     * @SWG\Property()
     */
    private ?int $verifyCallSent;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $createdAt;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $updatedAt;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $deletedAt;

    /**
     * @SWG\Property()
     */
    private ?string $directLink;

    /**
     * @SWG\Property()
     */
    private ?string $terId;

    /**
     * @SWG\Property()
     */
    private string $typeSlug;

    /**
     * @SWG\Property()
     */
    private ?int $terCheckStamp;

    /**
     * @SWG\Property()
     */
    private ?bool $terCheckPriority;

    /**
     * @SWG\Property()
     */
    private ?int $terCheckUnapproved;

    /**
     * @SWG\Property()
     */
    private ?int $verificationId;

    /**
     * @SWG\Property()
     */
    private ?int $verifiedStamp;

    /**
     * @SWG\Property()
     */
    private ?int $verifiedBy;

    public function __construct(Escort $escort, EscortLocationData ...$locations)
    {
        $this->bodyType = null !== $escort->getBodyType() ? BodyType::valueToName(
            $escort->getBodyType()
        ) : null;
        $this->bodyTypeRawValue = null !== $escort->getBodyType() ? $escort->getBodyType()->getRawValue() : null;
        $this->id = $escort->getId();
        $this->stateId = $escort->getStateId();
        $this->type = EscortTypeConverter::valueToName($escort->getType()->getRawValue());
        $this->rawType = $escort->getType()->getRawValue();
        $this->isShemale = $escort->isShemale();

        $this->typeSlug = EscortTypeConverter::valueToSlug($escort->getType()->getRawValue());

        $this->locations = $locations;
        $this->locationIds = $escort->getLocationIds();

        $this->accountId = $escort->getAccountId();
        $this->status = $escort->getStatus() ? $escort->getStatus()->getRawValue() : null;
        $this->statusInString = $escort->getStatus() ? StatusConverter::valueToName($escort->getStatus()->getRawValue()) : null;
        $this->expireStamp = $escort->getExpireStamp();
        $this->ethnicity = $escort->getEthnicity() !== null ? Ethnicity::valueToName($escort->getEthnicity()) : null;
        $this->ethnicityRawValue = $escort->getEthnicity() !== null ? $escort->getEthnicity()->getRawValue() : null;
        $this->phone = $escort->getPhone();
        $this->email = $escort->getEmail() ?: null;
        $this->emailVisibility = $escort->getEmailVisibility()->getRawValue();
        $this->website = $escort->getWebsite();
        $this->title = $escort->getTitle();
        $this->placeDescription = $escort->getPlaceDescription();
        $this->postedAt = $escort->getPostedAt();
        $this->content = $escort->getContent();
        $this->thumbnail = $escort->getThumbnail();
        $this->multipleThumbnails = $escort->getMultipleThumbnails();

        $this->isIncallEnabled = $escort->isIncallEnabled();
        $this->incallRateHalfHour = $escort->getIncallRateHalfHour();
        $this->incallRateOneHour = $escort->getIncallRateOneHour();
        $this->incallRateTwoHours = $escort->getIncallRateTwoHours();
        $this->incallRateOvernight = $escort->getIncallRateOvernight();
        $this->incallRate = $escort->getIncallRate();

        $this->isOutcallEnabled = $escort->isOutcallEnabled();
        $this->outcallRateHalfHour = $escort->getOutcallRateHalfHour();
        $this->outcallRateOneHour = $escort->getOutcallRateOneHour();
        $this->outcallRateTwoHours = $escort->getOutcallRateTwoHours();
        $this->outcallRateOvernight = $escort->getOutcallRateOvernight();
        $this->outcallRate = $escort->getOutcallRate();

        $this->available = $escort->getAvailable();

        $this->facebook = $escort->getFacebook();
        $this->twitter = $escort->getTwitter();
        $this->telegram = $escort->getTelegram();
        $this->google = $escort->getGoogle();
        $this->instagram = $escort->getInstagram();
        $this->wechat = $escort->getWechat();
        $this->whatsapp = $escort->getWhatsapp();

        $this->age = $escort->getAge();
        $this->isPornstar = $escort->getIsPornstar();

        $this->isVisaAccepted = $escort->isVisaAccepted();
        $this->isAmexAccepted = $escort->isAmexAccepted();
        $this->isDiscoverAccepted = $escort->isDiscoverAccepted();

        $this->isVisiting = $escort->isVisiting();
        $this->visitingFrom = $escort->getVisitingFrom();
        $this->visitingTo = $escort->getVisitingTo();

        $this->hairColor = $escort->getHairColor() !== null ? HairColor::valueToName($escort->getHairColor()) : null;
        $this->hairColorRawValue = $escort->getHairColor() !== null ? $escort->getHairColor()->getRawValue() : null;
        $this->eyeColor = $escort->getEyeColor() !== null ? EyeColor::valueToName($escort->getEyeColor()) : null;
        $this->eyeColorRawValue = $escort->getEyeColor() !== null ? $escort->getEyeColor()->getRawValue() : null;
        $this->isPregnant = $escort->getIsPregnant();
        $this->tantra = $escort->getTantra();

        $this->firstName = $escort->getFirstName();
        $this->middleName = $escort->getMiddleName();
        $this->lastName = $escort->getLastName();

        $this->repostLeft = $escort->getRepostLeft();
        $this->autoRenew = $escort->getAutoRenew();
        $this->autoRenewFrequency = $escort->getAutoRenewFrequency();
        $this->autoRenewTime = $escort->getAutoRenewTime();

        $this->sponsor = $escort->getSponsor();
        $this->sponsorMobile = $escort->getSponsorMobile();
        $this->sponsorPosition = $escort->getSponsorPosition();
        $this->sponsorLocId = $escort->getSponsorLocId();

        $this->allowLinks = $escort->getAllowLinks();
        $this->isAvailableForMen = $escort->isAvailableForMen();
        $this->isAvailableForWomen = $escort->isAvailableForWomen();
        $this->isAvailableForCouple = $escort->isAvailableForCouple();
        $this->isAvailableForGroup = $escort->isAvailableForGroup();
        $this->isAvailableForBlack = $escort->isAvailableForBlack();

        $this->fetishDominant = $escort->getFetishDominant();
        $this->fetishSubmissive = $escort->getFetishSubmissive();
        $this->fetishSwitch = $escort->getFetishSwitch();

        $this->heightFeet = $escort->getHeightFeet();
        $this->heightInches = $escort->getHeightInches();
        $this->weight = $escort->getWeight();
        $this->bust = $escort->getBust();
        $this->waist = $escort->getWaist();
        $this->hip = $escort->getHip();
        $this->cupSize = null !== $escort->getCupSize() ? CupSize::valueToName(\App\Escorts\Domain\CupSize::getValueOf($escort->getCupSize())) : null;
        $this->cupSizeRawValue = $escort->getCupSize();
        $this->penisSize = $escort->getPenisSize();
        $this->kitty = null !== $escort->getKitty() ? Kitty::valueToName($escort->getKitty()) : null;
        $this->kittyRawValue = null !== $escort->getKitty() ? $escort->getKitty()->getRawValue() : null;
        $this->fetish = $escort->getFetish();
        $this->gfe = $escort->getGfe();
        $this->gfeLimited = $escort->getGfeLimited();

        $this->verified = $escort->getVerified();
        $this->verifyCallSent = $escort->getVerifyCallSent();

        $this->languageIds = array_map(
            static function (Language $language): int {
                return (int)$language->getRawValue();
            },
            $escort->getLanguageIds()
        );

        $this->images = array_map(
            static function (EscortImage $image): string {
                return $image->getFilename();
            },
            $escort->getImages()
        );

        $this->videos = array_map(
            static function (EscortVideo $video): string {
                return $video->getFilename();
            },
            $escort->getVideos()
        );

        $this->createdAt = $escort->getCreatedAt();
        $this->updatedAt = $escort->getUpdatedAt();
        $this->deletedAt = $escort->getDeletedAt();

        $this->directLink = $escort->getDirectLink();
        $this->terId = $escort->getTerId();
        $this->terCheckStamp = $escort->getTerCheckStamp();
        $this->terCheckPriority = $escort->getTerCheckPriority();
        $this->terCheckUnapproved = $escort->getTerCheckUnapproved();
        $this->verificationId = $escort->getVerificationId();
        $this->verifiedStamp = $escort->getVerifiedStamp();
        $this->verifiedBy = $escort->getVerifiedBy();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getStateId(): ?int
    {
        return $this->stateId;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getRawType(): int
    {
        return $this->rawType;
    }

    public function isShemale(): bool
    {
        return $this->isShemale;
    }

    /**
     * @return EscortLocationData[]
     */
    public function getLocations(): array
    {
        return $this->locations;
    }

    /**
     * @return int[]
     */
    public function getLocationIds(): array
    {
        return $this->locationIds;
    }

    public function getAccountId(): ?int
    {
        return $this->accountId;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function getStatusInString(): ?string
    {
        return $this->statusInString;
    }

    public function getExpireStamp(): ?\DateTimeImmutable
    {
        return $this->expireStamp;
    }

    public function getEthnicity(): ?string
    {
        return $this->ethnicity;
    }

    public function getEthnicityRawValue(): ?int
    {
        return $this->ethnicityRawValue;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getEmailVisibility(): int
    {
        return $this->emailVisibility;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPlaceDescription(): ?string
    {
        return $this->placeDescription;
    }

    public function getPostedAt(): DateTimeImmutable
    {
        return $this->postedAt;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function getMultipleThumbnails(): ?string
    {
        return $this->multipleThumbnails;
    }

    public function getIsIncallEnabled(): bool
    {
        return $this->isIncallEnabled;
    }

    public function getIncallRateHalfHour(): int
    {
        return $this->incallRateHalfHour;
    }

    public function getIncallRateOneHour(): int
    {
        return $this->incallRateOneHour;
    }

    public function getIncallRateTwoHours(): int
    {
        return $this->incallRateTwoHours;
    }

    public function getIncallRateOvernight(): int
    {
        return $this->incallRateOvernight;
    }

    public function getIncallRate(): ?int
    {
        return $this->incallRate;
    }

    public function getIsOutcallEnabled(): bool
    {
        return $this->isOutcallEnabled;
    }

    public function getOutcallRateHalfHour(): int
    {
        return $this->outcallRateHalfHour;
    }

    public function getOutcallRateOneHour(): int
    {
        return $this->outcallRateOneHour;
    }

    public function getOutcallRateTwoHours(): int
    {
        return $this->outcallRateTwoHours;
    }

    public function getOutcallRateOvernight(): int
    {
        return $this->outcallRateOvernight;
    }

    public function getOutcallRate(): ?int
    {
        return $this->outcallRate;
    }

    public function getAvailable(): ?int
    {
        return $this->available;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function getTelegram(): ?string
    {
        return $this->telegram;
    }

    public function getGoogle(): ?string
    {
        return $this->google;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function getWechat(): ?string
    {
        return $this->wechat;
    }

    public function getWhatsapp(): ?string
    {
        return $this->whatsapp;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function getIsPornstar(): bool
    {
        return $this->isPornstar;
    }

    public function getIsVisaAccepted(): bool
    {
        return $this->isVisaAccepted;
    }

    public function getIsAmexAccepted(): bool
    {
        return $this->isAmexAccepted;
    }

    public function getIsDiscoverAccepted(): bool
    {
        return $this->isDiscoverAccepted;
    }

    public function getIsVisiting(): bool
    {
        return $this->isVisiting;
    }

    public function getVisitingFrom(): ?DateTimeImmutable
    {
        return $this->visitingFrom;
    }

    public function getVisitingTo(): ?DateTimeImmutable
    {
        return $this->visitingTo;
    }

    public function getHairColor(): ?string
    {
        return $this->hairColor;
    }

    public function getRawHairColor(): ?int
    {
        return $this->hairColorRawValue;
    }

    public function getEyeColor(): ?string
    {
        return $this->eyeColor;
    }

    public function getRawEyeColor(): ?int
    {
        return $this->eyeColorRawValue;
    }

    public function getIsPregnant(): bool
    {
        return $this->isPregnant;
    }

    public function getTantra(): bool
    {
        return $this->tantra;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getRepostLeft(): int
    {
        return $this->repostLeft;
    }

    public function getAutoRenew(): ?bool
    {
        return $this->autoRenew;
    }

    public function getAutoRenewFrequency(): ?int
    {
        return $this->autoRenewFrequency;
    }

    public function getAutoRenewTime(): ?int
    {
        return $this->autoRenewTime;
    }

    public function getSponsor(): bool
    {
        return $this->sponsor;
    }

    public function getSponsorMobile(): bool
    {
        return $this->sponsorMobile;
    }

    public function getSponsorPosition(): ?int
    {
        return $this->sponsorPosition;
    }

    public function getSponsorLocId(): ?int
    {
        return $this->sponsorLocId;
    }

    public function getAllowLinks(): bool
    {
        return $this->allowLinks;
    }

    public function getIsAvailableForMen(): bool
    {
        return $this->isAvailableForMen;
    }

    public function getIsAvailableForWomen(): bool
    {
        return $this->isAvailableForWomen;
    }

    public function getIsAvailableForCouple(): bool
    {
        return $this->isAvailableForCouple;
    }

    public function getIsAvailableForGroup(): bool
    {
        return $this->isAvailableForGroup;
    }

    public function getIsAvailableForBlack(): bool
    {
        return $this->isAvailableForBlack;
    }

    public function getFetishDominant(): bool
    {
        return $this->fetishDominant;
    }

    public function getFetishSubmissive(): bool
    {
        return $this->fetishSubmissive;
    }

    public function getFetishSwitch(): bool
    {
        return $this->fetishSwitch;
    }

    public function getHeightFeet(): int
    {
        return $this->heightFeet;
    }

    public function getHeightInches(): int
    {
        return $this->heightInches;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function getBodyType(): ?string
    {
        return $this->bodyType;
    }

    public function getBodyTypeRawValue(): ?int
    {
        return $this->bodyTypeRawValue;
    }

    public function getBust(): int
    {
        return $this->bust;
    }

    public function getWaist(): int
    {
        return $this->waist;
    }

    public function getHip(): int
    {
        return $this->hip;
    }

    public function getCupSize(): ?string
    {
        return $this->cupSize;
    }

    public function getCupSizeRawValue(): int
    {
        return $this->cupSizeRawValue;
    }

    public function getPenisSize(): ?int
    {
        return $this->penisSize;
    }

    public function getKitty(): ?string
    {
        return $this->kitty;
    }

    public function getKittyRawValue(): ?int
    {
        return $this->kittyRawValue;
    }

    public function getFetish(): ?string
    {
        return $this->fetish;
    }

    public function getGfe(): bool
    {
        return $this->gfe;
    }

    public function getGfeLimited(): bool
    {
        return $this->gfeLimited;
    }

    /**
     * @return int[]
     */
    public function getLanguageIds(): array
    {
        return $this->languageIds;
    }

    /**
     * @return string[]
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @return string[]
     */
    public function getVideos(): array
    {
        return $this->videos;
    }

    public function getVerified(): bool
    {
        return $this->verified;
    }

    public function getVerifyCallSent(): ?int
    {
        return $this->verifyCallSent;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getDeletedAt(): ?DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function getDirectLink(): ?string
    {
        return $this->directLink;
    }

    public function getTerId(): ?string
    {
        return $this->terId;
    }

    public function getTerCheckStamp(): ?int
    {
        return $this->terCheckStamp;
    }

    public function getTerCheckPriority(): ?bool
    {
        return $this->terCheckPriority;
    }

    public function getTerCheckUnapproved(): ?int
    {
        return $this->terCheckUnapproved;
    }

    public function getVerificationId(): ?int
    {
        return $this->verificationId;
    }

    public function getVerifiedStamp(): ?int
    {
        return $this->verifiedStamp;
    }

    public function getVerifiedBy(): ?int
    {
        return $this->verifiedBy;
    }

    public function getTypeSlug(): string
    {
        return $this->typeSlug;
    }
}
