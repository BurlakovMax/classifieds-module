<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use Ramsey\Uuid\UuidInterface;
use Swagger\Annotations as SWG;

final class CreateEscortResultData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private ?string $linkToken;

    public function __construct(int $id, ?UuidInterface $linkToken)
    {
        $this->id = $id;
        $this->linkToken = null !== $linkToken ? $linkToken->toString() : null;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLinkToken(): ?string
    {
        return $this->linkToken;
    }
}
