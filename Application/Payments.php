<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\ApplicationSupport\ListOfEnumerable;
use Ds\Map;

final class Payments extends ListOfEnumerable
{
    public static function getList(): Map
    {
        $map = new Map();

        $map->put('isVisaAccepted', 'VISA - Mastercard');
        $map->put('isAmexAccepted', 'AMEX');
        $map->put('isDiscoverAccepted', 'Discover Card');

        return $map;
    }
}