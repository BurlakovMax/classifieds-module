<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\EscortType;
use LazyLemurs\Commander\Property;

final class CreateEscortCommand extends EscortCommand
{
    /**
     * @Property()
     */
    private int $type;

    /**
     * @var int[]
     */
    private array $cityThumbnailLocation = [];

    /**
     * @var int[]
     */
    private array $sideSponsorLocation = [];

    /**
     * @var int[]
     */
    private array $stickySponsorWeek = [];

    /**
     * @var int[]
     */
    private array $stickySponsorMoth = [];

    public function getType(): EscortType
    {
        return EscortType::getValueOf($this->type);
    }

    /**
     * @return int[]
     */
    public function getCityThumbnailLocation(): array
    {
        return $this->cityThumbnailLocation;
    }

    /**
     * @param int[]
     */
    public function setCityThumbnailLocation(array $cityThumbnailLocation): void
    {
        $this->cityThumbnailLocation = $cityThumbnailLocation;
    }

    /**
     * @return int[]
     */
    public function getSideSponsorLocation(): array
    {
        return $this->sideSponsorLocation;
    }

    /**
     * @param int[]
     */
    public function setSideSponsorLocation(array $sideSponsorLocation): void
    {
        $this->sideSponsorLocation = $sideSponsorLocation;
    }

    /**
     * @return int[]
     */
    public function getStickySponsorWeek(): array
    {
        return $this->stickySponsorWeek;
    }

    /**
     * @param int[]
     */
    public function setStickySponsorWeek(array $stickySponsorWeek): void
    {
        $this->stickySponsorWeek = $stickySponsorWeek;
    }

    /**
     * @return int[]
     */
    public function getStickySponsorMoth(): array
    {
        return $this->stickySponsorMoth;
    }

    /**
     * @param int[]
     */
    public function setStickySponsorMoth(array $stickySponsorMoth): void
    {
        $this->stickySponsorMoth = $stickySponsorMoth;
    }
}
