<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\EscortSponsor;
use App\Escorts\Domain\EscortType;
use App\Escorts\Domain\Status;
use Swagger\Annotations as SWG;

final class EscortSponsorData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private int $locationId;

    /**
     * @SWG\Property()
     */
    private int $stateId;

    /**
     * @SWG\Property()
     */
    private int $escortId;

    /**
     * @SWG\Property()
     */
    private int $expireInDays;

    /**
     * @SWG\Property()
     */
    private EscortType $type;

    /**
     * @SWG\Property()
     */
    private string $typeInString;

    /**
     * @SWG\Property()
     */
    private \DateTimeImmutable $expireAt;

    /**
     * @SWG\Property()
     */
    private Status $status;

    /**
     * @SWG\Property()
     */
    private ?string $statusInString;

    public function __construct(EscortSponsor $escortSponsor)
    {
        $this->id = $escortSponsor->getId();
        $this->locationId = $escortSponsor->getLocationId();
        $this->stateId = $escortSponsor->getStateId();
        $this->escortId = $escortSponsor->getEscortId();
        $this->expireInDays = $escortSponsor->getExpireInDays();
        $this->type = $escortSponsor->getType();
        $this->typeInString = EscortTypeConverter::valueToName($escortSponsor->getType()->getRawValue());
        $this->expireAt = $escortSponsor->getExpireAt();
        $this->status = $escortSponsor->getStatus();
        $this->statusInString = StatusConverter::valueToName($escortSponsor->getStatus()->getRawValue());
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getStateId(): int
    {
        return $this->stateId;
    }

    public function getEscortId(): int
    {
        return $this->escortId;
    }

    public function getExpireInDays(): int
    {
        return $this->expireInDays;
    }

    public function getType(): EscortType
    {
        return $this->type;
    }

    public function getTypeInString(): string
    {
        return $this->typeInString;
    }

    public function getExpireAt(): \DateTimeImmutable
    {
        return $this->expireAt;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function getStatusInString(): ?string
    {
        return $this->statusInString;
    }
}