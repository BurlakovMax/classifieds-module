<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\ApplicationSupport\ListOfEnumerable;
use App\Escorts\Domain\EyeColor as Enum;
use Ds\Map;

final class EyeColor extends ListOfEnumerable
{
    public static function getList(): Map
    {
        $map = new Map();

        $map->put(Enum::notSet(), 'Not-set');
        $map->put(Enum::blue(), 'Blue');
        $map->put(Enum::brown(), 'Brown');
        $map->put(Enum::green(), 'Green');
        $map->put(Enum::hazel(), 'Hazel');
        $map->put(Enum::black(), 'Black');
        $map->put(Enum::gray(), 'Gray');

        return $map;
    }

    public static function getRawListWithValue(): array
    {
        $enums = [
            Enum::notSet(),
            Enum::blue(),
            Enum::brown(),
            Enum::green(),
            Enum::hazel(),
            Enum::black(),
            Enum::gray(),
        ];

        $types = [];
        foreach ($enums as $enum) {
            if (Enum::notSet()->equals($enum)) {
                continue;
            }

            $types[] = [
                'value' => $enum->getRawValue(),
                'name' => static::getList()->get($enum),
            ];
        }

        return $types;
    }
}