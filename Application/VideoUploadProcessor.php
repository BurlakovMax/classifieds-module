<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\Application\CacheProcessor;
use App\Escorts\Domain\FileUpload;
use App\Escorts\Domain\FileUploaded;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Messenger\MessageBusInterface;

final class VideoUploadProcessor
{
    private const TTL = 3600;

    private string $rootDir;

    private string $publicDir;

    private string $targetDir;

    private string $uploadDir;

    private FileUpload $fileUpload;

    private MessageBusInterface $bus;

    private CacheProcessor $cache;

    public function __construct(
        string $rootDir,
        string $publicDir,
        string $targetDir,
        string $uploadDir,
        FileUpload $fileUpload,
        MessageBusInterface $bus,
        CacheProcessor $cache
    ) {
        $this->rootDir = $rootDir;
        $this->publicDir = $publicDir;
        $this->targetDir = $targetDir;
        $this->uploadDir = $uploadDir;
        $this->fileUpload = $fileUpload;
        $this->bus = $bus;
        $this->cache = $cache;
    }

    public function save(string $name, UploadedFile $uploadedFile): VideoUploadData
    {
        $id = Uuid::uuid4()->toString();
        $pathToVideoFile = $this->fileUpload->save($name, $uploadedFile);
        $this->cache->writeToCacheWithTTL($id, $name, self::TTL);
        $this->bus->dispatch(
            new FileUploaded($name, $pathToVideoFile, $this->targetDir)
        );

        return new VideoUploadData(
            $name . '.mp4',
            $name . '.jpg',
            $this->targetDir . $name . '.mp4',
            $this->targetDir . $name . '.jpg',
            $id
        );
    }

    /**
     * @throws VideoUploadedNotFound
     */
    public function remove(string $id): void
    {
        $video = $this->cache->readFromCache($id);

        if (null === $video) {
            throw new VideoUploadedNotFound();
        }

        if (file_exists($this->rootDir . $this->publicDir . $this->uploadDir . $video . '.mp4')) {
            unlink($this->rootDir . $this->publicDir . $this->uploadDir . $video . '.mp4');
        }

        if (file_exists($this->rootDir . $this->publicDir . $this->targetDir . $video . '.mp4')) {
            unlink($this->rootDir . $this->publicDir . $this->targetDir . $video . '.mp4');
        }

        if (file_exists($this->rootDir . $this->publicDir . $this->targetDir . $video . '.jpg')) {
            unlink($this->rootDir . $this->publicDir . $this->targetDir . $video . '.jpg');
        }
    }

    public function removeByName(string $name): void
    {
        if (file_exists($this->rootDir . $this->publicDir . $this->targetDir . $name)) {
            unlink($this->rootDir . $this->publicDir . $this->targetDir . $name);
        }
    }
}