<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\EscortType;
use App\Escorts\Domain\EscortWaitingList;
use App\Escorts\Domain\EscortWaitingListWriteStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class EscortWaitingListCommandProcessor
{
    private TransactionManager $transactionManager;

    private EscortWaitingListWriteStorage $escortWaitingListWriteStorage;

    public function __construct(
        TransactionManager $transactionManager,
        EscortWaitingListWriteStorage $escortWaitingListWriteStorage
    ) {
        $this->transactionManager = $transactionManager;
        $this->escortWaitingListWriteStorage = $escortWaitingListWriteStorage;
    }

    /**
     * @throws \Throwable
     */
    public function create(int $accountId, int $locationId, int $escortTypeRaw): void
    {
        $this->transactionManager->transactional(function () use ($accountId, $locationId, $escortTypeRaw): void {
            $this->escortWaitingListWriteStorage->add(
                new EscortWaitingList(
                    $accountId,
                    $locationId,
                    EscortType::getValueOf($escortTypeRaw)
                )
            );
        });
    }
}